﻿using System;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class AmsInfoItem {
		public string AmsName { get; set; }
		public Guid AmsId { get; set; }

		public override string ToString() {
			return AmsName;
		}
	}
}