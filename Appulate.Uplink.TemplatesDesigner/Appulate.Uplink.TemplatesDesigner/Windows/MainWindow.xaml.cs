﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;
using System.Windows.Input;
using Appulate.Domain.Entities;
using Appulate.Domain.Entities.Supplementary;
using Appulate.Shared.Pdf;
using Appulate.Uplink.Consts;
using Appulate.Uplink.Extraction.ParseDocument;
using Appulate.Uplink.Extraction.Pdf;
using Appulate.Uplink.Extraction.Pdf.PdfNet;
using Appulate.Uplink.Templates;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.Templates.Serialization;
using Appulate.Uplink.Templates.Serialization.Ams;
using Appulate.Uplink.TemplatesDesigner.Configuration;
using Appulate.Uplink.TemplatesDesigner.Controls;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Appulate.Utilities.Misc;
using Appulate.Utilities.State;
using Microsoft.Win32;
using Brushes = System.Windows.Media.Brushes;
using InputType = Appulate.Uplink.Templates.Data.InputType;
using Settings = Appulate.Business.Settings;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	public partial class MainWindow {
		private const string FileFilter = "XML Files|*.xml|All Files|*.*";

		private readonly MyDocument _toolDocument;
		private readonly RectangleSelection _selection;
		private Dictionary<string, DocumentFormatTemplate<PageFormatTemplate>> _amsDocuments;
		private List<UplinkTemplateProperties> _currentAmsDocuments;
		private Dictionary<Guid, string> _templates;
		private AmsList _amsList;
		private DataGridRow _selectedRow;
		private int _docsCurrntPageNumber = 1;
		private int _totalRecordsCount;
		private int _totalPagesCount;
		private int _docsPageSize;

		private List<UplinkTemplateProperties> CurrentAmsDocuments {
			get => _currentAmsDocuments;
			set {
				_currentAmsDocuments = value;
				_totalRecordsCount = _currentAmsDocuments.Count;
				_totalPagesCount = _totalRecordsCount / _docsPageSize + (_totalRecordsCount % _docsPageSize > 0 ? 1 : 0);
			}
		}

		enum PageAction {
			First = 1,
			Next,
			Previous,
			Last
		}

		public MainWindow() {
			AbcPdfWrapper.Touch();
			InitializeComponent();
			LoadComboBoxes();

			_toolDocument = new MyDocument(myCanvas);
			myCanvas.SetDocument(_toolDocument);
			_selection = myCanvas.Selection;
			if (new ConnectionWindows().ShowDialog() == true) {
				LocalSettings.Instance.Initialize();
				_docsPageSize = int.Parse(ConfigurationManager.AppSettings["DocsPageSize"]);
				lblDocsCount.Content = _docsPageSize;
				dgAmsDocuments.MouseDoubleClick += (s, a) => OpenGridDocument();
			} else {
				Close();
			}
		}

		#region Helper routines

		private void LoadComboBoxes() {
			cbMappingMode.ItemsSource = EnumUtils.All<MappingMode>();
			cbInputType.ItemsSource = EnumUtils.All<InputType>();
		}

		private void LoadDocument() {
			while (miPages.Items.Count > 2) {
				miPages.Items.RemoveAt(1);
			}
			for (int i = 2; i <= _toolDocument.PageCount; i++) {
				var mi = new RibbonMenuItem { Header = i };
				mi.Click += miPagesNumber_Click;
				miPages.Items.Insert(i - 1, mi);
			}
			MetaInformation metaInfo = _toolDocument.MetaInfo;
			if (metaInfo.CorrectionPoint == null) {
				chbCorrectionPoint.IsChecked = false;
				txtCorrectionPointX.Text = string.Empty;
				txtCorrectionPointY.Text = string.Empty;
				txtPageHeight.Text = string.Empty;
				txtCorrectionText.Text = string.Empty;
			} else {
				chbCorrectionPoint.IsChecked = true;
				txtCorrectionPointX.Text = metaInfo.CorrectionPoint.X.ToString();
				txtCorrectionPointY.Text = metaInfo.CorrectionPoint.Y.ToString();
				txtPageHeight.Text = metaInfo.CorrectionPoint.PageHeight.ToString();
				txtCorrectionText.Text = metaInfo.CorrectionPoint.Text;
			}
			miPagesNumber_Click(miPages.Items[0], null);
			statusFileName.Content = _toolDocument.FileName;
			BindCurrentdocumentProperties();
		}

		private void BindCurrentdocumentProperties() {
			UplinkTemplateProperties currentProperties = _toolDocument.Properties;
			if (currentProperties == null) {
				return;
			}
			tbName.Text = currentProperties.Name;
			tbDocProducer.Text = currentProperties.Producer;
			tbDocProducerEmail.Text = currentProperties.ProducerEmail;
			cbDocState.SelectedItem = cbDocState.Items.Cast<ComboBoxItem>()
			                                    .FirstOrDefault(x => ((StateInfoComboBoxItem)x.Content).StateId.Equals(currentProperties.State, StringComparison.InvariantCultureIgnoreCase));
			cbDocStatus.SelectedValue = cbDocStatus.Items.Cast<ComboBoxItem>().FirstOrDefault(x => ((UplinkTemplateStatus)x.Content) == (UplinkTemplateStatus)currentProperties.Status);
			tbDocSyncId.Text = currentProperties.SyncId.ToString().ToLowerInvariant();
			cbDocTemplate.SelectedValue = cbDocTemplate.Items.Cast<ComboBoxItem>().FirstOrDefault(x => ((TemplateInfoComboBoxItem)x.Content).TemplateId == currentProperties.TemplateId);
			dpDocVersion.SelectedDate = currentProperties.Version;
			cbDocType.SelectedValue = cbDocType.Items.Cast<ComboBoxItem>().FirstOrDefault(x => (UplinkTemplateType)x.Content == currentProperties.Type);

			IEnumerable<AmsInfoItem> docAmsCollection = currentProperties.UplinkAmsList.Select(x => {
				AmsListItem item = _amsList.Items.Single(y => y.Id == x);
				return new AmsInfoItem { AmsId = x, AmsName = item.Name + " " + item.Version };
			});
			ComboItemsBinder.BindEnumerableButtonsItems(lsbDocAmsList, docAmsCollection);
			foreach (object item in lsbDocAmsList.Items) {
				((Button)item).Click += lbsRemoveItem;
			}
		}

		private void FillComboBoxData() {
			List<AmsInfoItem> amsList = _amsList.Items.Select(x => new AmsInfoItem { AmsId = x.Id, AmsName = x.Name + " " + x.Version })
			                                    .OrderBy(x => x.AmsName).ToList();
			List<TemplateInfoComboBoxItem> templateList = _templates.Select(x => new TemplateInfoComboBoxItem(x.Value, x.Key))
			                                                        .OrderBy(x => {
				                                                        int value;
				                                                        return int.TryParse(string.Concat(x.TemplateName.Where(char.IsDigit)), out value) ? value : 0;
			                                                        })
			                                                        .ThenBy(x => string.Concat(x.TemplateName.Where(c => !char.IsDigit(c)))).ToList();
			ComboItemsBinder.BindEnumItems(cbDocStatus, typeof(UplinkTemplateStatus));
			ComboItemsBinder.BindEnumItems(cbDocType, typeof(UplinkTemplateType));
			ComboItemsBinder.BindEnumerableComboBoxItems(cbDocTemplate, templateList);
			ComboItemsBinder.BindEnumerableComboBoxItems(cbAddAms, amsList);
			ComboItemsBinder.BindEnumerableComboBoxItems(cbDocState, StatesCache.AllStates.Select(x => new StateInfoComboBoxItem { StateName = $"({x.Id}) {x.Name}", StateId = x.Id }));
			ComboItemsBinder.BindEnumItems(cbSearchByTemplateType, typeof(UplinkTemplateType));
			ComboItemsBinder.BindEnumerableComboBoxItems(cbSearchByAmsName, amsList);
			ComboItemsBinder.BindEnumerableComboBoxItems(cbSearchByTemplateName, templateList);
			cbDocState.Items.Insert(0, new ComboBoxItem { Content = new StateInfoComboBoxItem { StateName = "None", StateId = string.Empty } });
			cbSearchByTemplateType.Items.Insert(0, new ComboBoxItem { Content = "All" });
			cbSearchByTemplateName.Items.Insert(0, new ComboBoxItem { Content = new TemplateInfoComboBoxItem("All", Guid.Empty) });
			cbSearchByAmsName.Items.Insert(0, new ComboBoxItem { Content = new AmsInfoItem { AmsId = Guid.Empty, AmsName = "All" } });
			cbSearchByTemplateType.SelectedIndex = 0;
			cbSearchByAmsName.SelectedIndex = 0;
			cbSearchByTemplateName.SelectedIndex = 0;
		}

		private bool SaveDocument() {
			try {
				UplinkTemplateProperties properties = CreateProperties();
				if (properties == null) {
					return false;
				}
				_toolDocument.Properties = properties;

				string templatePath = GetFolderPath(properties);
				SaveDocumentAs(Path.Combine(templatePath, UplinkFiles.FormatFile), miUpdateOcr.IsChecked.Value);
				MyMessageBox.SimpleMessage("Successfully saved in " + templatePath);
				return true;
			} catch (Exception ex) {
				MyMessageBox.Error("An error occurred saving\n" + ex.Message, ex.InnerException);
				return false;
			}
		}

		private bool SaveDocumentAs() {
			try {
				var fileDlg = new SaveFileDialog { Filter = FileFilter };
				return fileDlg.ShowDialog() == true && SaveDocumentAs(fileDlg.FileName, miUpdateOcr.IsChecked.Value);
			} catch (Exception ex) {
				MyMessageBox.Error("An error occurcurrentSyncIdDocPathred saving\n" + ex.Message, ex.InnerException);
				return false;
			}
		}

		private bool SaveDocumentAs(string fileName, bool updateOcr) {
			MetaInformation metaInfo = _toolDocument.MetaInfo;
			if (chbCorrectionPoint.IsChecked == true) {
				if (metaInfo.CorrectionPoint == null) {
					metaInfo.CorrectionPoint = new CorrectionPoint();
				}
				CorrectionPoint correctionPoint = metaInfo.CorrectionPoint;
				correctionPoint.X = float.Parse(txtCorrectionPointX.Text);
				correctionPoint.Y = float.Parse(txtCorrectionPointY.Text);
				correctionPoint.PageHeight = float.Parse(txtPageHeight.Text);
				correctionPoint.Text = txtCorrectionText.Text;
			} else {
				metaInfo.CorrectionPoint = null;
			}
			_toolDocument.Save(fileName, updateOcr);
			statusFileName.Content = _toolDocument.FileName;
			_amsDocuments[Path.GetDirectoryName(fileName)] = XmlUtility.FromXmlFile<DocumentFormatTemplate<PageFormatTemplate>>(fileName);
			return true;
		}

		private void OpenGridDocument() {
			if (dgAmsDocuments.SelectedIndex >= 0) {
				if (SaveDocumentIfModified() == MessageBoxResult.Cancel) {
					return;
				}
				var prop = (dgAmsDocuments.Items[dgAmsDocuments.SelectedIndex] as UplinkTemplateProperties);
				string filePath = Path.Combine(_amsDocuments.First(x => x.Value.Properties.SyncId == prop.SyncId).Key, UplinkFiles.FormatFile);
				if (OpenDocument(filePath)) {
					if (_selectedRow != null) {
						_selectedRow.Background = Brushes.White;
					}
					_selectedRow = dgAmsDocuments.ItemContainerGenerator.ContainerFromItem(prop) as DataGridRow;
					if (_selectedRow != null) {
						_selectedRow.Background = Brushes.LightGray;
					}
				}
			}
		}

		private bool OpenDocument() {
			var fileDlg = new OpenFileDialog { Filter = FileFilter };
			return fileDlg.ShowDialog() == true && OpenDocument(fileDlg.FileName);
		}

		private bool OpenDocument(string filePath) {
			try {
				_toolDocument.Load(filePath);
				SwitchPropertiesControlsEnabledState(true);
				LoadDocument();
				EnableMappingEditingControls(false);
				txtMappingText.Text = tbMappingId.Text = string.Empty;
				return true;
			} catch (Exception ex) {
				MyMessageBox.Warning(ex.Message);
				return false;
			}
		}

		private void AcceptProperties() {
			if (_toolDocument.Properties == null) {
				return;
			}
			UplinkTemplateProperties modifiedProperties = CreateProperties();
			if (modifiedProperties == null) {
				return;
			}
			if (!_toolDocument.Properties.Equals(modifiedProperties)) {
				_toolDocument.Properties = modifiedProperties;
				_toolDocument.IsModified = true;
			}
		}

		private MessageBoxResult SaveDocumentIfModified() {
			var result = MessageBoxResult.None;
			AcceptProperties();
			if (_toolDocument.IsModified) {
				string fileName = string.IsNullOrEmpty(_toolDocument.FileName) ? "Untitled" : _toolDocument.FileName;
				result = MyMessageBox.Exclaim($"The content of the document {fileName} has changed.\nDo you want save the changes?");
				if (result == MessageBoxResult.Yes && !SaveDocument()) {
					result = MessageBoxResult.Cancel;
				}
			}
			return result;
		}

		private IEnumerable<UplinkTemplateProperties> FilterTemplatesList(string syncIdString, Guid templateId, Guid amsId, UplinkTemplateType? type) {
			return _amsDocuments.Select(x => x.Value.Properties).Where(x => {
				bool result = (x.SyncId.ToString().StartsWith(syncIdString, StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrEmpty(syncIdString)) &&
				              (x.TemplateId == templateId || templateId == Guid.Empty) &&
				              (x.UplinkAmsList.Any(z => z == amsId) || amsId == Guid.Empty) &&
				              (!type.HasValue || x.Type == type);
				return result;
			});
		}

		// Create a Properties object from Gui elements
		private UplinkTemplateProperties CreateProperties() {
			try {
				return new UplinkTemplateProperties {
					Name = tbName.Text,
					ProducerEmail = tbDocProducerEmail.Text,
					Producer = tbDocProducer.Text,
					State = ((StateInfoComboBoxItem)(((ComboBoxItem)cbDocState.SelectedValue).Content)).StateId,
					Status = (int)((UplinkTemplateStatus)(((ComboBoxItem)cbDocStatus.SelectedValue).Content)),
					SyncId = new Guid(tbDocSyncId.Text),
					TemplateId = ((TemplateInfoComboBoxItem)((ComboBoxItem)cbDocTemplate.SelectedValue).Content).TemplateId,
					Version = dpDocVersion.SelectedDate.Value,
					UplinkAmsList =
						new System.Collections.ObjectModel.Collection<Guid>(
							lsbDocAmsList.Items.Cast<Button>().Select(x => ((AmsInfoItem)(x.Content)).AmsId).ToList()),
					Type = (UplinkTemplateType)((ComboBoxItem)cbDocType.SelectedValue).Content
				};
			} catch (Exception ex) {
				MyMessageBox.Error("Failed to create properties, check fields\n" + ex.Message);
				return null;
			}
		}

		private void LoadCurrentPage() {
			List<RibbonMenuItem> items = miPages.Items.OfType<RibbonMenuItem>().ToList();
			foreach (RibbonMenuItem item in items) {
				item.IsChecked = false;
			}
			items[_toolDocument.CurrentPageIndex].IsChecked = true;
			scrollCanvas.ScrollToTop();
			lblFoundCount.Content = myCanvas.MakeInvisibleRectanglesWithOutText(txtMappingSearch.Text);
		}

		private void SwitchPropertiesControlsEnabledState(bool enabledState) {
			myCanvas.IsEnabled = rgEdit.IsEnabled = grMapEdit.IsEnabled = rtAdvanced.IsEnabled
				                                                              = miQAFileSave.IsEnabled = miFileSave.IsEnabled = miFileSaveAs.IsEnabled = enabledState;
			foreach (object uiElement in ((Grid)tbItmProperties.Content).Children) {
				if (!(uiElement is Label)) {
					((UIElement)uiElement).IsEnabled = enabledState;
				}
			}

			if (!enabledState) {
				ClearPropertyControls();
			}
		}

		private void ClearPropertyControls() {
			tbDocProducer.Text = tbDocProducerEmail.Text = tbDocSyncId.Text = tbName.Text = string.Empty;
			cbDocTemplate.SelectedIndex = cbDocState.SelectedIndex = cbDocStatus.SelectedIndex = -1;
			dpDocVersion.SelectedDate = null;
			lsbDocAmsList.Items.Clear();
			myCanvas.ClearDocument();
			_toolDocument.Properties = null;
		}

		private void AfterSelectionPerformed() {
			MyRectangle[] selectionItems = _selection.Items.ToArray();
			int count = selectionItems.Length;
			if (count == 0) {
				EnableMappingEditingControls(false);
				tbXmlPath.Text = txtMappingText.Text = tbMappingId.Text = string.Empty;
			} else if (count == 1) {
				cbMappingMode.SelectedValue = selectionItems[0].MappingMode;
				cbInputType.SelectedValue = selectionItems[0].InputType;
				txtMappingText.Text = selectionItems[0].MappingText;
				tbXmlPath.Text = selectionItems[0].XmlPath;
				tbMappingId.Text = selectionItems[0].Id.ToString();
				EnableMappingEditingControls(true);
			} else {
				cbInputType.IsEnabled = cbMappingMode.IsEnabled = true;
				cbInputType.DropDownClosed += cbInputType_DropDownClosed;
				cbMappingMode.DropDownClosed += cbMappingMode_DropDownClosed;
				txtMappingText.IsEnabled = false;
				tbXmlPath.IsEnabled = false;
				txtMappingText.TextChanged -= txtMappingText_TextChanged;
				tbXmlPath.TextChanged -= txtXmlPath_TextChanged;
			}
		}

		private void EnableMappingEditingControls(bool enable) {
			cbInputType.IsEnabled = cbMappingMode.IsEnabled = txtMappingText.IsEnabled = tbXmlPath.IsEnabled = enable;
			if (enable) {
				cbInputType.DropDownClosed += cbInputType_DropDownClosed;
				cbMappingMode.DropDownClosed += cbMappingMode_DropDownClosed;
				txtMappingText.TextChanged += txtMappingText_TextChanged;
				tbXmlPath.TextChanged += txtXmlPath_TextChanged;
			} else {
				cbInputType.DropDownClosed -= cbInputType_DropDownClosed;
				cbMappingMode.DropDownClosed -= cbMappingMode_DropDownClosed;
				txtMappingText.TextChanged -= txtMappingText_TextChanged;
				tbXmlPath.TextChanged -= txtXmlPath_TextChanged;
			}
		}

		private void SetPage() {
			if (_totalRecordsCount <= _docsPageSize) {
				dgAmsDocuments.ItemsSource = CurrentAmsDocuments;
			} else {
				dgAmsDocuments.ItemsSource = CurrentAmsDocuments.ToList().GetRange((_docsCurrntPageNumber - 1) * _docsPageSize,
				                                                                   (_docsCurrntPageNumber == _totalPagesCount && _totalRecordsCount % _docsPageSize > 0)
					                                                                   ? _totalRecordsCount % _docsPageSize
					                                                                   : _docsPageSize);
			}
		}

		#endregion

		#region EventHandlers

		private void cbMappingMode_DropDownClosed(object sender, EventArgs e) {
			if (_selection != null && cbMappingMode.SelectedValue != null) {
				_selection.ChangeMode((MappingMode)cbMappingMode.SelectedValue);
			}
		}

		private void cbInputType_DropDownClosed(object sender, EventArgs e) {
			if (_selection != null && cbInputType.SelectedValue != null) {
				_selection.ChangeInputType((InputType)cbInputType.SelectedValue);
			}
		}

		private void txtMappingSearch_TextChanged(object sender, TextChangedEventArgs e) {
			lblFoundCount.Content = myCanvas.MakeInvisibleRectanglesWithOutText(txtMappingSearch.Text);
		}

		private void txtMappingText_TextChanged(object sender, TextChangedEventArgs e) {
			_selection.ChangeText(txtMappingText.Text);
		}

		private void txtXmlPath_TextChanged(object sender, TextChangedEventArgs e) {
			_selection.ChangeXmlPath(tbXmlPath.Text);
		}

		private void chbCorrectionPoint_Click(object sender, RoutedEventArgs e) {
			bool enabled = chbCorrectionPoint.IsChecked == true;
			txtCorrectionPointX.IsEnabled = enabled;
			txtCorrectionPointY.IsEnabled = enabled;
			txtPageHeight.IsEnabled = enabled;
			txtCorrectionText.IsEnabled = enabled;
			btnLoadCp.IsEnabled = enabled;
		}

		private void btnEdit_Click(object sender, EventArgs e) {
			OpenGridDocument();
		}

		private void dgAmsDocuments_Sorting(object sender, DataGridSortingEventArgs e) {
			e.Handled = true;
			ListSortDirection direction = e.Column.SortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
			bool sortAscending = direction == ListSortDirection.Ascending;
			switch (e.Column.SortMemberPath) {
				case "Type":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.Type) : CurrentAmsDocuments.OrderByDescending(x => x.Type)).ToList();
					break;
				case "Name":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.Name) : CurrentAmsDocuments.OrderByDescending(x => x.Name)).ToList();
					break;
				case "Producer":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.Producer) : CurrentAmsDocuments.OrderByDescending(x => x.Producer)).ToList();
					break;
				case "State":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.State) : CurrentAmsDocuments.OrderByDescending(x => x.State)).ToList();
					break;
				case "Status":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.Status) : CurrentAmsDocuments.OrderByDescending(x => x.Status)).ToList();
					break;
				case "SyncId":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.SyncId) : CurrentAmsDocuments.OrderByDescending(x => x.SyncId)).ToList();
					break;
				case "TemplateId":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.TemplateId) : CurrentAmsDocuments.OrderByDescending(x => x.TemplateId)).ToList();
					break;
				case "AmsList":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.UplinkAmsList) : CurrentAmsDocuments.OrderByDescending(x => x.UplinkAmsList)).ToList();
					break;
				case "Version":
					CurrentAmsDocuments = (sortAscending ? CurrentAmsDocuments.OrderBy(x => x.Version) : CurrentAmsDocuments.OrderByDescending(x => x.Version)).ToList();
					break;
			}
			SetPage();
			dgAmsDocuments.Columns[e.Column.DisplayIndex].SortDirection = direction;
		}

		private void Window_ContentRendered(object sender, EventArgs e) {
			IsEnabled = false;
			_amsList = AmsList.Load(Path.Combine(LoadingControl.UplinkTemplatesDirectory, UplinkFiles.SupportedAmsFile));
			//_templates = Factory.Resolve<DocumentDesignerTemplateRepository>().GetAllTemplatesForUplinkDocuments();
			_templates = GetAllTemplatesForUplinkDocuments();
			FillComboBoxData();
			loadingControl.Load();
			_amsDocuments = loadingControl.Documents;
			CurrentAmsDocuments = _amsDocuments.Select(x => x.Value.Properties).ToList();
			ChangePage(PageAction.First);
			SwitchPropertiesControlsEnabledState(false);
			IsEnabled = true;
			EnableMappingEditingControls(false);
			chbCorrectionPoint_Click(null, null);
		}

		private void miFileOpen_Click(object sender, RoutedEventArgs e) {
			if (SaveDocumentIfModified() == MessageBoxResult.Cancel) {
				return;
			}
			if (OpenDocument()) {
				if (_selectedRow != null) {
					_selectedRow.Background = Brushes.White;
				}
			}
			e.Handled = true;
		}

		private void miFileSave_Click(object sender, RoutedEventArgs e) {
			SaveDocument();
			e.Handled = true;
		}

		private void miFileSaveAs_Click(object sender, RoutedEventArgs e) {
			SaveDocumentAs();
			e.Handled = true;
		}

		private void miPagesNumber_Click(object sender, RoutedEventArgs e) {
			_toolDocument.CurrentPageIndex = Convert.ToInt32(((RibbonMenuItem)sender).Header) - 1;
			LoadCurrentPage();
		}

		private void miPagesAdd_Click(object sender, RoutedEventArgs e) {
			_toolDocument.AddPage();
			var newMenuItem = new RibbonMenuItem { Header = _toolDocument.PageCount };
			newMenuItem.Click += miPagesNumber_Click;
			miPages.Items.Insert(miPages.Items.Count - 1, newMenuItem);
			miPagesNumber_Click(newMenuItem, null);
			e.Handled = true;
		}

		private void btnRemoveCurrentPage_Click(object sender, RoutedEventArgs e) {
			if (MyMessageBox.Question("Are you sure you want to remove current page?") == MessageBoxResult.OK) {
				_toolDocument.RemoveCurrentPage();
				if (miPages.Items.Count > 2) {
					miPages.Items.RemoveAt(miPages.Items.Count - 2);
				}
				LoadCurrentPage();
			}
			e.Handled = true;
		}

		private void miDevelopmentPicture_Click(object sender, RoutedEventArgs e) {
			var fileDlg = new OpenFileDialog();
			if (_toolDocument != null) {
				fileDlg.InitialDirectory = _toolDocument.GetImagesFolder();
			}
			bool? result = fileDlg.ShowDialog();
			if (!result.Value) {
				return;
			}
			try {
				_toolDocument.CurrentPage.DevelopmentPicture = fileDlg.FileName;
				myCanvas.LoadDevelopmentBitmapImage(_toolDocument.CurrentPage.DevelopmentBitmap);
				e.Handled = true;
			} catch {
				MessageBox.Show("Incorrect image");
			}
		}

		private void myCanvas_SelectedRectanglesChanged(object sender, EventArgs e) {
			AfterSelectionPerformed();
		}

		private void lbsRemoveItem(object sender, EventArgs e) {
			lsbDocAmsList.Items.Remove(sender);
		}

		private void cbAddAms_DropDownClosed(object sender, EventArgs e) {
			if (cbAddAms.SelectedValue != null) {
				if (lsbDocAmsList.Items.Cast<Button>().All(x => ((AmsInfoItem)x.Content).AmsId != ((AmsInfoItem)((ComboBoxItem)cbAddAms.SelectedValue).Content).AmsId)) {
					var button = new Button {
						Width = lsbDocAmsList.Width - ComboItemsBinder.ScrollBarWidth,
						Content = new AmsInfoItem
							{ AmsId = ((AmsInfoItem)((ComboBoxItem)cbAddAms.SelectedValue).Content).AmsId, AmsName = ((AmsInfoItem)((ComboBoxItem)cbAddAms.SelectedValue).Content).AmsName }
					};
					button.Click += lbsRemoveItem;
					lsbDocAmsList.Items.Add(button);
				}
			}
		}

		private void miAmsEditorClick(object sender, RoutedEventArgs e) {
			var amsEditorWindow = new AmsEditorWindow(_amsList);
			amsEditorWindow.ShowDialog();
			e.Handled = true;
		}

		private void btNewGuid_Click(object sender, RoutedEventArgs e) {
			Guid newGuid = Guid.NewGuid();
			while (_amsDocuments.Any(x => x.Value.Properties.SyncId == newGuid)) {
				newGuid = Guid.NewGuid();
			}
			tbDocSyncId.Text = newGuid.ToString().ToLowerInvariant();
			e.Handled = true;
		}

		private void btFilter_Click(object sender, RoutedEventArgs e) {
			CurrentAmsDocuments = FilterTemplatesList(tbSearchSyncId.Text,
			                                          ((TemplateInfoComboBoxItem)((ComboBoxItem)cbSearchByTemplateName.SelectedValue).Content).TemplateId,
			                                          ((AmsInfoItem)((ComboBoxItem)cbSearchByAmsName.SelectedValue).Content).AmsId,
			                                          ((ComboBoxItem)cbSearchByTemplateType.SelectedValue).Content as UplinkTemplateType?).ToList();
			ChangePage(PageAction.First);
		}

		private void btResetFilter_Click(object sender, RoutedEventArgs e) {
			CurrentAmsDocuments = _amsDocuments.Select(x => x.Value.Properties).ToList();
			cbSearchByTemplateName.SelectedIndex = 0;
			cbSearchByAmsName.SelectedIndex = 0;
			cbSearchByTemplateType.SelectedIndex = 0;
			tbSearchSyncId.Text = string.Empty;
			ChangePage(PageAction.First);
			e.Handled = true;
		}

		private void miFileNew_Click(object sender, RoutedEventArgs e) {
			if (SaveDocumentIfModified() != MessageBoxResult.Cancel) {
				var newDocumentWindow = new NewDocumentWindow();
				newDocumentWindow.ShowDialog();
			}
			e.Handled = true;
		}

		private void btnLoadCp_Click(object sender, RoutedEventArgs e) {
			string imagefolder = _toolDocument.GetImagesFolder();
			if (!Directory.Exists(imagefolder)) {
				MyMessageBox.Error("Document not found");
			} else {
				string pdfFileName = Path.Combine(imagefolder, _toolDocument.Properties.SyncId + ".pdf");
				if (File.Exists(pdfFileName)) {
					byte[] content = File.ReadAllBytes(pdfFileName);
					using (PdfExtractedDocument doc = PdfExtractFactory.CreateDocument(content)) {
						var document = new PdfRetriveDocument(doc);
						PointF? point = CorrectionPointOffset.ReadOffset(document.Pages[0], txtCorrectionText.Text);
						if (point != null) {
							txtCorrectionPointX.Text = point.Value.X.ToString(CultureInfo.InvariantCulture);
							txtCorrectionPointY.Text = point.Value.Y.ToString(CultureInfo.InvariantCulture);
							txtPageHeight.Text = doc.Pages[0].Height.ToString(CultureInfo.InvariantCulture);
						} else {
							MyMessageBox.Error(string.Format(txtCorrectionText.Text + " not found."));
						}
					}
				} else {
					MyMessageBox.Error($"File {pdfFileName} not found");
				}
			}
			e.Handled = true;
		}

		private void miSearchMapping_Click(object sender, RoutedEventArgs e) {
			var searchWindow = new WindowSearch { Documents = _amsDocuments };
			searchWindow.Show();
			e.Handled = true;
		}

		private void miExamineCheckpoints_Click(object sender, RoutedEventArgs e) {
			var window = new ExamineCheckPointsWindow(_amsDocuments.Values.Select(x =>
				                                                                      new AcordTemplateComboBoxItem
					                                                                      { Name = string.Concat(x.Properties.Name, '_', x.Properties.State).Trim('_'), SyncId = x.Properties.SyncId })
			                                                       .OrderBy(x => x.Name));
			Guid syncId = Guid.Empty;
			if (_toolDocument.Properties != null) {
				syncId = _toolDocument.Properties.SyncId;
			} else {
				MyMessageBox.SimpleMessage("There is no opened document. Open the document or load PDF file from disk to recognize");
			}
			window.Init(syncId);
			window.ShowDialog();
			e.Handled = true;
		}

		private void miFlowSectionsEditor_Click(object sender, RoutedEventArgs e) {
			var window = new FlowSectionsEditorWindow();
			window.Show();
			e.Handled = true;
		}

		private void tbItmDocuments_PreviewKeyDown(object sender, KeyEventArgs e) {
			if (e.Key == Key.Enter) {
				btFilter_Click(null, null);
			}
		}

		private void miParserResources_Click(object sender, RoutedEventArgs e) {
			var window = new ParserResourcesWindow();
			window.Show();
			e.Handled = true;
		}

		private void miExit_Click(object sender, RoutedEventArgs e) {
			Close();
		}

		private void window_Closing(object sender, CancelEventArgs e) {
			e.Cancel = SaveDocumentIfModified() == MessageBoxResult.Cancel;
			Properties.Settings ps = Properties.Settings.Default;
			ps.LastTop = Top;
			ps.LastLeft = Left;
			ps.LastWidth = Width;
			ps.LastHeight = Height;
			ps.Save();
		}

		private void wMain_Loaded(object sender, RoutedEventArgs e) {
			Properties.Settings ps = Properties.Settings.Default;
			if (ps.LastWidth == 0 && ps.LastHeight == 0) {
				return;
			}
			Top = ps.LastTop;
			Left = ps.LastLeft;
			Width = ps.LastWidth;
			Height = ps.LastHeight;
		}

		#region Paging Actions

		private void btnPrev_Click(object sender, RoutedEventArgs e) {
			ChangePage(PageAction.Previous);
			e.Handled = true;
		}

		private void btnFirst_Click(object sender, RoutedEventArgs e) {
			ChangePage(PageAction.First);
			e.Handled = true;
		}

		private void btnNext_Click(object sender, RoutedEventArgs e) {
			ChangePage(PageAction.Next);
			e.Handled = true;
		}

		private void btnLast_Click(object sender, RoutedEventArgs e) {
			ChangePage(PageAction.Last);
			e.Handled = true;
		}

		private void ChangePage(PageAction action) {
			switch (action) {
				case PageAction.First:
					_docsCurrntPageNumber = 1;
					break;
				case PageAction.Next:
					_docsCurrntPageNumber++;
					break;
				case PageAction.Previous:
					_docsCurrntPageNumber--;
					break;
				case PageAction.Last:
					_docsCurrntPageNumber = _totalPagesCount;
					break;
			}
			SetPage();
			btnNext.IsEnabled = btnLast.IsEnabled = _totalRecordsCount > _docsCurrntPageNumber * _docsPageSize;
			btnPrev.IsEnabled = btnFirst.IsEnabled = _docsCurrntPageNumber > 1;
			lbCurrentPageNumber.Content = $"Page {_docsCurrntPageNumber} of {_totalPagesCount}";
		}

		private void ChangeDocumentsPerPageCount(int countDiff) {
			_docsPageSize += countDiff;
			System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
			config.AppSettings.Settings["DocsPageSize"].Value = _docsPageSize.ToString();
			config.Save(ConfigurationSaveMode.Modified);
			_totalPagesCount = _totalRecordsCount / _docsPageSize + (_totalRecordsCount % _docsPageSize > 0 ? 1 : 0);
			lblDocsCount.Content = _docsPageSize;
			ChangePage(PageAction.First);
		}

		private void btnIncDocsCount_Click(object sender, RoutedEventArgs e) {
			ChangeDocumentsPerPageCount(1);
			e.Handled = true;
		}

		private void btnDecDocsCount_Click(object sender, RoutedEventArgs e) {
			if (_docsPageSize == 1) {
				MyMessageBox.Warning("Documents on page count can't be less than 1");
				return;
			}
			ChangeDocumentsPerPageCount(-1);
			e.Handled = true;
		}

		#endregion

		#endregion

		private void CloseSearch_Click(object sender, RoutedEventArgs e) {
			spSearch.Visibility = Visibility.Hidden;
			txtMappingSearch.Clear();
		}

		private void wMain_KeyDown(object sender, KeyEventArgs e) {
			if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.F) {
				spSearch.Visibility = Visibility.Visible;
				txtMappingSearch.Focus();
			}
		}

		private void RibbonButton_Click(object sender, RoutedEventArgs e) {
			var syncId = new Guid(tbDocSyncId.Text);
			UplinkTemplateProperties prop = _amsDocuments.FirstOrDefault(x => x.Value.Properties.SyncId == syncId).Value?.Properties;
			string path = GetFolderPath(prop, getTemplate: false);
			Process.Start(path);
		}

		public static string GetFileName(string name, string state, Guid syncId) {
			string str = string.IsNullOrEmpty(state) ? string.Empty : state + "_";
			return name.Trim().Replace(" ", "_") + "_" + str + syncId.ToString().ToUpperInvariant();
		}

		public static string GetFilePath(string name, string state, Guid syncId) {
			return Path.Combine(LoadingControl.UplinkTemplatesDirectory, GetFileName(name, state, syncId));
		}

		private string GetFolderPath(UplinkTemplateProperties prop, bool getTemplate = true) {
			string resourcePath;
			if (prop == null) {
				prop = CreateProperties();
			}
			string templatePath = _amsDocuments.FirstOrDefault(x => ReferenceEquals(x.Value.Properties, prop)).Key;
			if (string.IsNullOrEmpty(templatePath)) {
				templatePath = GetFilePath(prop.Name, prop.State, prop.SyncId);
				if (!Directory.Exists(templatePath)) {
					Directory.CreateDirectory(templatePath);
				}
				string folderName = Path.GetFileName(templatePath);
				resourcePath = Path.Combine(LoadingControl.UplinkTemplatesResourcesDirectory, folderName);
				if (!Directory.Exists(resourcePath)) {
					Directory.CreateDirectory(resourcePath);
				}
			} else {
				resourcePath = Path.Combine(LoadingControl.UplinkTemplatesResourcesDirectory, Path.GetFileName(templatePath));
			}
			return getTemplate ? templatePath : resourcePath;
		}

		private Dictionary<Guid, string> GetAllTemplatesForUplinkDocuments() {
			return new Dictionary<Guid, string> {
				{ new Guid("35bcb067-f01a-4340-9985-00dc99db9d17"), "ACORD 127]" },
				{ new Guid("4fcf5fd0-7555-4ca7-911c-00fa5e407ff5"), "ACORD 149]" },
				{ new Guid("cf86f632-1b5d-4c03-ba5e-02c2841a016c"), "ACORD 301]" },
				{ new Guid("006182e2-b2ef-40e2-addd-04661e950b18"), "ACORD 137 M]" },
				{ new Guid("da868b0f-cb77-40ea-beab-04e67b6fc441"), "ACORD 137 I]" },
				{ new Guid("9e28847a-2a76-4e6a-8171-08291db93954"), "ACORD 138 N]" },
				{ new Guid("b2dc2388-61de-4d85-89e0-088a61100122"), "ACORD 290 M]" },
				{ new Guid("d060a3a0-0525-47a5-85a6-08e8f08a6870"), "ACORD 137 W]" },
				{ new Guid("036a8c83-d111-483f-9eb9-0a24feaaed8c"), "ACORD 823]" },
				{ new Guid("b7adbd69-e67d-40f1-a0de-0ae448cd477d"), "ACORD 140]" },
				{ new Guid("61a5b46f-ae67-4331-b653-0bcceb240543"), "ACORD 137 N]" },
				{ new Guid("c94b2247-b023-498a-9288-13dc3ddae799"), "ACORD 90 NV" },
				{ new Guid("7af97e5f-8058-404c-812e-145e95303871"), "ACORD 137 UT]" },
				{ new Guid("3c8019f0-7958-4df5-b32b-14bf3614f6d6"), "Accident Fund(WC)-Default]" },
				{ new Guid("e9b7b037-e891-4f53-9651-1677b2ac90fb"), "ACORD 834 AR]" },
				{ new Guid("19d068c4-c994-4ae8-8417-195cac6e3ff0"), "ACORD 90 NM]" },
				{ new Guid("328884fa-1e89-4332-a6ca-19b0783ce9ce"), "ACORD 90 UT]" },
				{ new Guid("6c6230b8-b3d2-4db6-b193-1a1b0d75056a"), "ACORD 138 SD]" },
				{ new Guid("c9371c17-40a8-40f7-aa28-1c1f33689d54"), "ACORD 827]" },
				{ new Guid("95010499-bb0a-4d8f-a56b-1cda57b2923c"), "ACORD 137 GA]" },
				{ new Guid("0a51362c-f5c2-4b3b-8685-1cdd23d18b0f"), "ACORD 4]" },
				{ new Guid("73dc0762-d84a-45be-9952-1e05d7ecf03c"), "ACORD 90 CT]" },
				{ new Guid("dfc16091-d76f-4344-b213-1eb8cd12e0d4"), "ACORD 402]	" },
				{ new Guid("300f935d-577c-48f8-9b91-1ff83bdcf161"), "ACORD 137 AR]" },
				{ new Guid("657997e6-a2ed-4a6d-8431-20d8a1af6ba1"), "ACORD 125 FL]" },
				{ new Guid("36711002-d302-48c3-a606-2655326a3753"), "ACORD 137 NE]" },
				{ new Guid("3847fc94-c6c9-4e75-8678-26a5f81e78ed"), "ACORD 152]" },
				{ new Guid("56a22d0b-ac0b-45ff-8ac9-26d91583115e"), "ACORD 137 RI]" },
				{ new Guid("19a537da-8189-4c6f-b1c5-28176b517bdd"), "ACORD 45]" },
				{ new Guid("e6160dec-a68c-43f6-ab13-2850fb393229"), "ACORD 137 ID]" },
				{ new Guid("12092bc2-7ae4-47eb-92ba-2d509db9db99"), "ACORD 137 MI]" },
				{ new Guid("230123b0-4f59-4577-81d2-2de05b056a5e"), "ACORD 133 WI]" },
				{ new Guid("db0fadb2-3ae5-4779-9177-2e15d39f2bdb"), "ACORD 90 TX]" },
				{ new Guid("c9bf8f6d-280b-4644-a642-2edea41850be"), "ACORD 137 SD]" },
				{ new Guid("43ad8470-9efd-438a-84a7-30083e717463"), "ACORD 403]" },
				{ new Guid("a400cad5-7edf-4393-ad41-30a0078ebf78"), "ACORD 137 DC]" },
				{ new Guid("3febcf7b-7fb0-47bc-b4ff-30a1477de888"), "ACORD 137 CA]" },
				{ new Guid("0d91a4f4-3e3e-4e80-b2de-3164c7de3bce"), "ACORD 137 MD]" },
				{ new Guid("b982ae28-2dc5-4c9d-8fdc-3295c794a415"), "ACORD 810]" },
				{ new Guid("528ff7dc-2554-4fbd-9171-338291970107"), "ACORD 90 AK]" },
				{ new Guid("c9cf9a38-0498-4b61-ab2f-34e555204175"), "ACORD 138 CO]" },
				{ new Guid("9192057e-1e86-4b90-b8fd-373213474599"), "ACORD 137 AL]" },
				{ new Guid("4e8ff9db-3d3c-4815-a891-37ac5962f1e1"), "ACORD 137 MT]" },
				{ new Guid("ed4b2603-70e1-45fe-ac64-37b4c09fadd9"), "ACORD 827 MN]" },
				{ new Guid("979ca68f-ce10-4fbb-b0e9-3874cbb7d870"), "ACORD 138 AZ]" },
				{ new Guid("30e06c66-659d-4fe4-ba10-38fe055603bb"), "ACORD 137 MA]" },
				{ new Guid("399cd11e-947b-4c10-992e-3b2ea64c07f2"), "ACORD 133]" },
				{ new Guid("5b67e5fe-931b-4768-b8fc-3bd1279fe049"), "ACORD 137 NJ]" },
				{ new Guid("cc3d6c4b-23b2-496a-9058-3f67d26c401c"), "ACORD 137 IA]" },
				{ new Guid("ae84c699-fa48-4c6a-8289-3fb71b4a632e"), "ACORD 133 MI]" },
				{ new Guid("baf0321f-4fc4-4819-a78c-40380028b4ea"), "ACORD 80]" },
				{ new Guid("9ea0c379-2be3-4dcf-a21f-403c95c4935c"), "ACORD 90 VT]" },
				{ new Guid("f7acf3c4-e679-4486-a599-41f774242e60"), "ACORD 137 NV]" },
				{ new Guid("67ed0ea6-e6d0-49c2-b9cc-44cb1548b715"), "ACORD 90 OR]" },
				{ new Guid("032be828-8aed-4fbf-941a-4631b6a56dfd"), "ACORD 138 NV]" },
				{ new Guid("ccf2a340-095c-486e-a5d6-474800151055"), "ACORD 803]" },
				{ new Guid("3b2040ef-03ee-4c19-8abd-4c5fc9aaa53a"), "ACORD 138 CA]" },
				{ new Guid("c058c5d3-40c1-4321-a74f-5306cd06bd17"), "ACORD 90 AL]" },
				{ new Guid("db1a13a5-ea9e-480b-8eed-55231e04bbf2"), "ACORD 90 OH]" },
				{ new Guid("6994f882-a3f7-4ae4-b4c6-5752a95c84d0"), "ACORD 138 ND]" },
				{ new Guid("584464c3-0533-47e9-955d-5d9b91a7f4bf"), "ACORD 137 OK]" },
				{ new Guid("4edab296-d285-4f0f-893c-5e2d59a2388a"), "ACORD 138 IN]" },
				{ new Guid("8aeabfc4-e2c4-411f-917a-5e9f3440675c"), "ACORD 138 FL]" },
				{ new Guid("0bea222b-93e6-407f-8719-60050f80c3bf"), "ACORD 137 WA]" },
				{ new Guid("df745f0e-b104-44ad-a7d0-622e9d911629"), "ACORD 83]" },
				{ new Guid("da5c2dd8-d371-41a4-8c80-63a0ef26d032"), "ACORD 42]" },
				{ new Guid("9805fcf4-a365-4db9-be8b-65200acc478e"), "ACORD 138 OR]" },
				{ new Guid("e254a728-342c-420b-9f24-6665201a55d1"), "ACORD 815]" },
				{ new Guid("3c582e5e-26f8-4140-ba49-6758c1d02640"), "ACORD 193]" },
				{ new Guid("be312e57-7a25-4d13-b7a3-675d6785e827"), "ACORD 137 MN]" },
				{ new Guid("72895722-3462-4e62-9a7c-6932987ea6b2"), "ACORD 190]" },
				{ new Guid("44d30503-ba0c-4477-acf8-69a9641afb71"), "ACORD 137 DE]" },
				{ new Guid("bc5939d0-9792-4abe-9339-6c55216b7d7b"), "ACORD 211]" },
				{ new Guid("e78f72ef-4c26-4fce-b317-6d4445515145"), "ACORD 834]" },
				{ new Guid("c5a48b30-0d84-46b1-86ef-6d60f7803325"), "ACORD 137 MS]" },
				{ new Guid("1ce4b6d8-4200-45cf-96da-6e9265cffffb"), "ACORD 146]" },
				{ new Guid("be4c7baa-fc3f-40e0-aecc-6ed3e479b8f3"), "ACORD 138 NM]" },
				{ new Guid("6f05bc61-5117-459f-aa62-6ee3f959b102"), "ACORD 138 MO]" },
				{ new Guid("8f05bc61-5117-459f-aa62-6ee9f959b102"), "ACORD 138 NE]" },
				{ new Guid("0c1e3b47-d800-4c1a-9eac-6f8f384a50da"), "ACORD 137 VA]" },
				{ new Guid("58ad73d4-3623-4f12-ba25-70beb174b07e"), "ACORD 90 ME]" },
				{ new Guid("dd8639df-d07c-4264-a10e-720e0b389cea"), "ACORD 137 NC]" },
				{ new Guid("3d68543e-3e82-422b-8e51-7374025478f4"), "ACORD 137 SC]" },
				{ new Guid("70bdf9a9-385f-46fa-8443-75a2767ec640"), "ACORD 406]" },
				{ new Guid("01939108-1a9b-4ded-96f4-765cd0d0d5c0"), "ACORD 194]" },
				{ new Guid("fb86f244-c680-4449-82fe-7764e9c7eb98"), "ACORD 25]" },
				{ new Guid("d517e523-89a4-4cb1-b156-7a245dd8c8fb"), "ACORD 90 RI]" },
				{ new Guid("48080b1d-82fc-4059-bf55-7a514e702240"), "ACORD 3]" },
				{ new Guid("174a0bc1-0fde-4c68-a3c5-7e2cb13eb011"), "ACORD 807]" },
				{ new Guid("b4a56474-cf5b-4890-99a9-7f37d5ea0bdc"), "ACORD 90 NH]" },
				{ new Guid("4a1312b9-8230-45ca-81e5-7f8c6c2e76ca"), "ACORD 827 MT]" },
				{ new Guid("155f0d29-1551-4e6c-87a8-7fa3427ce57d"), "ACORD 135 NC]" },
				{ new Guid("3de583c3-496c-4a80-9471-85ed93877ab0"), "ACORD 138 GA]" },
				{ new Guid("49944ea3-3ad4-427f-ad87-87edb92823a4"), "ACORD 825]" },
				{ new Guid("e0b02e38-d4ac-4b16-b958-88de2ae3cdfc"), "ACORD 90 MO]" },
				{ new Guid("d2718f3c-da0d-43e9-8319-8afa0a7944b8"), "ACORD 128]" },
				{ new Guid("65d37a8b-902e-4ba1-9524-8b99fbb915be"), "ACORD 130 FL]" },
				{ new Guid("8ea3cff7-0615-48f3-a63a-8cabe64b592d"), "ACORD 188]" },
				{ new Guid("3dd8717f-7ae9-44e5-82db-8cb4d6863e4a"), "ACORD 130 CA]" },
				{ new Guid("40649b08-725f-4d03-ac57-8ccf6bcb4fc5"), "ACORD 133 NJ]" },
				{ new Guid("b50ac4aa-ed39-4cd9-8388-8ee3be3f0d3d"), "ACORD 90 FL]" },
				{ new Guid("395f9e8d-aef4-41ba-ab38-8f1a03f49d09"), "ACORD 133 TN]" },
				{ new Guid("10ea34fd-622c-4ba9-8ad6-8fc9fcef552a"), "ACORD 187]" },
				{ new Guid("7ce2014c-a59f-4256-a636-91314cc33e56"), "ACORD 160]" },
				{ new Guid("7a84f8fb-aeec-4e8b-845b-925812278fb7"), "ACORD 138 KS]" },
				{ new Guid("a52bb42a-400c-465c-a998-930b34e7352c"), "ACORD 90 VA]" },
				{ new Guid("f884225c-4fbd-48e3-b688-948d26352b72"), "ACORD 90 GA]" },
				{ new Guid("9ab653e3-b753-4f76-ae6b-94e60e27baa2"), "ACORD 84]" },
				{ new Guid("dcb64428-de3c-4672-863d-9585f453c673"), "ACORD 145]" },
				{ new Guid("a33692cd-9c05-4ef2-a40e-985c8db41892"), "ACORD 137 KS]" },
				{ new Guid("7a51afe4-533f-4a1c-92ae-9936dcdcedd6"), "ACORD 81]" },
				{ new Guid("94357a3a-660e-45ea-9f8f-995deb501d12"), "ACORD 163]" },
				{ new Guid("9863a563-9c03-4eff-9b7d-9d93ee7ad99b"), "ACORD 99]" },
				{ new Guid("542e7819-b523-47b1-8df5-9f1032087ed4"), "ACORD 129]" },
				{ new Guid("7cc711d6-f319-4552-8607-9f1f4536651a"), "ACORD 90 IA]" },
				{ new Guid("6e1ebbe2-57ae-4004-abc0-9f5cd9ff59dd"), "ACORD 137 VT]" },
				{ new Guid("6fd43818-7d95-4fe6-9719-9f93d9c97165"), "ACORD 138 UT]" },
				{ new Guid("7d2d4b07-fce0-4154-861b-a3cf2a6ca4ee"), "ACORD 811]" },
				{ new Guid("c41af0cd-34bb-4020-b32a-a4368bcdb7ac"), "ACORD 138 IL]" },
				{ new Guid("eddde1fa-b140-41b9-8da4-a45e77b4219b"), "ACORD 138 WY]" },
				{ new Guid("ee0f0739-a2bb-464e-b23c-a591df88f3de"), "ACORD 90 MA]" },
				{ new Guid("957308c9-6b19-4448-b2d9-a6c0d8f1ae3a"), "ACORD 89]" },
				{ new Guid("aed3df66-8f72-4d4a-81b5-a74d496dca7c"), "ACORD 827 AR]" },
				{ new Guid("8a16cda8-bd6a-4ed0-a50b-a7a1440002da"), "ACORD 137 OR]" },
				{ new Guid("969158e1-2f76-455e-bb46-a7a1dbc6ad79"), "ACORD 106]" },
				{ new Guid("87b564dc-35ce-4390-9589-a949c8e3c9a7"), "ACORD 290 CA]" },
				{ new Guid("d83ca09d-f684-41d7-bfea-a9feb5ae9d00"), "ACORD 186]" },
				{ new Guid("e7e10e94-c27e-4bc1-996b-ab6afcf688f3"), "ACORD 802]" },
				{ new Guid("3aab706a-5b2c-408f-9f82-ab6f28179d2d"), "ACORD 131]" },
				{ new Guid("e6f0312a-1b69-4e5f-b5e1-ac45cbcb087f"), "ACORD 137 OH]" },
				{ new Guid("400d9a77-5a93-464d-81e3-ae4b24e46e86"), "ACORD 143]" },
				{ new Guid("4a664821-7b75-4919-a6a6-b1d2a036f045"), "ACORD 138 MN]" },
				{ new Guid("1d06e072-5d4f-4faa-9f91-b1ec2c337d93"), "ACORD 90 TN]" },
				{ new Guid("e2aaf0dd-91e8-4a41-b123-b299ba73d754"), "ACORD 141]" },
				{ new Guid("50298e1b-fd04-438d-be94-b3c8c29565f9"), "ACORD 138 KY]" },
				{ new Guid("9ec1314f-4e75-4552-93f4-b442f6887c49"), "ACORD 137 NH]" },
				{ new Guid("deaff954-4c46-46b5-bb6c-b575a7513cea"), "ACORD 138 NY]" },
				{ new Guid("36e27d9e-1cfe-49ea-a29f-b5f93e135faa"), "ACORD 101]" },
				{ new Guid("4c4c6dce-72d2-423f-86b1-b62a7e4ddda6"), "ACORD 801]" },
				{ new Guid("a4fbfb4f-2db8-47a4-bee6-b84e88910fe0"), "ACORD 125]" },
				{ new Guid("8390fb0a-3e49-4420-a126-b86ee1f0571c"), "ACORD 137 LA]" },
				{ new Guid("c2463057-e38a-4876-8559-b9e5b9ae65aa"), "ACORD 137 WY]" },
				{ new Guid("111c879f-b852-447f-a238-bd3286425920"), "ACORD 133 FL]" },
				{ new Guid("c7b5b7b8-bb3c-4794-852a-be37883cc726"), "ACORD 90 CO]" },
				{ new Guid("603d6a4b-b23b-40cc-b276-c10ddce616e5"), "ACORD 90 IN]" },
				{ new Guid("030b4e05-de63-4d0f-bbb1-c512fc658aad"), "ACORD 137 TN]" },
				{ new Guid("cbaa929d-8d0a-4b62-b408-c530b7a5ac0f"), "ACORD 137 CO]" },
				{ new Guid("bc3d1de4-cac7-4d6d-bb70-c5bffca76203"), "ACORD 2]" },
				{ new Guid("f8efeb69-331b-49ea-825a-c8b5109edfe5"), "ACORD 404]" },
				{ new Guid("d94aeeb7-f2b7-42aa-99cf-ca26bae7e00b"), "ACORD 401]" },
				{ new Guid("c184258f-542e-4c0c-9135-ccd6ed377640"), "ACORD 138 OH]" },
				{ new Guid("8f7a4d78-fea3-4106-af4a-ce8969f4a862"), "ACORD 90 IL]" },
				{ new Guid("df02bc1c-b913-4428-bd95-d0898b2202b4"), "ACORD 147]" },
				{ new Guid("136d321b-73b3-4d7d-ab22-d2c6f3c862f8"), "ACORD 64 US]" },
				{ new Guid("d4f91b8e-20c0-42f5-932a-d397ecae981e"), "ACORD 196]" },
				{ new Guid("63faaa70-52d6-4696-9bf1-d6be358215b7"), "ACORD 137 IL]" },
				{ new Guid("5200c3da-e67b-492c-a87d-d7d35a45b628"), "ACORD 90 CA]" },
				{ new Guid("982bd5bd-0ebe-49e1-b9a1-d86173aae299"), "ACORD 138 WI]" },
				{ new Guid("247be340-bd55-4c7c-91bd-d888871b925e"), "ACORD 1]" },
				{ new Guid("38bba91c-702f-44c3-acaf-db5b2ebcbe84"), "ACORD 410]" },
				{ new Guid("c890644b-7076-41ea-a9a9-dbe12151f508"), "ACORD 834 MN]" },
				{ new Guid("63cce2bf-9734-4a5c-82e0-dc167d02d290"), "ACORD 90 AZ]" },
				{ new Guid("8973feaa-dcb1-4f16-ba14-dcd725655cb3"), "ACORD 137 CT]" },
				{ new Guid("8e1ec549-ba98-4e33-af22-dd186faf12df"), "ACORD 137 WI]" },
				{ new Guid("6cd493b3-9bf0-4ca7-8430-e01324714c18"), "ACORD 834 MT]" },
				{ new Guid("8287d5dc-dabb-4b73-81a5-e10bed1af30d"), "ACORD 138 WA]" },
				{ new Guid("b4dd572b-ca0b-4e84-ac04-e296b0fbe987"), "ACORD 130]" },
				{ new Guid("8497929d-d415-4e61-8c80-e2fcaae5b704"), "ACORD 82]" },
				{ new Guid("54444a21-6409-4317-8060-e58336cfd793"), "ACORD 137 AK]" },
				{ new Guid("683d2187-e7d2-4804-90af-e5f1f89b827f"), "ACORD 137 ME]" },
				{ new Guid("5a3b0094-e9df-4bb2-aeef-e6274fb94234"), "ACORD 126]" },
				{ new Guid("d3976df7-5c95-43c1-9b1d-e76ed793bc2b"), "ACORD 180]" },
				{ new Guid("229d33df-ebda-4d37-9c1a-e82be9256ae0"), "ACORD 137 NY]" },
				{ new Guid("fa9c8e6f-e927-4ef0-80c5-e9670e693181"), "ACORD 137 KY]" },
				{ new Guid("38276da3-7894-4bcb-bf23-e9b0aaf477e9"), "ACORD 137 HI]" },
				{ new Guid("469af56e-f791-4578-9028-eabcd2988b62"), "ACORD 137 TX]" },
				{ new Guid("fa428ab1-128f-4856-8693-eadf9410a0bf"), "Keystone(BOP)-Default]" },
				{ new Guid("fef6a993-6b47-416e-9479-ec6ce8903ce8"), "ACORD 155 BM]" },
				{ new Guid("e9be9269-bbda-4b50-86af-edd22fff897c"), "ACORD 132]" },
				{ new Guid("744614c5-69c6-49a3-ad90-f02cdc6e6cf3"), "ACORD 137 AZ]" },
				{ new Guid("85bf33e1-8801-485f-89ac-f0a200c64566"), "ACORD 137 FL]" },
				{ new Guid("eb4948b9-3249-481d-9998-f13c35a17af9"), "ACORD 90 MI]" },
				{ new Guid("d40598ad-f824-45d6-a772-f1f2806736b8"), "ACORD 138 IA]" },
				{ new Guid("bd46d5bc-22c3-4e19-b421-f275967702b4"), "ACORD 88]" },
				{ new Guid("d6649373-1a06-456c-917f-f2980015884e"), "ACORD 408]" },
				{ new Guid("6a5596b9-4675-4a87-acc9-f3e0b4dcafa9"), "ACORD 148]" },
				{ new Guid("17045a42-4f5e-4845-87c0-f541df2d9324"), "ACORD 90 WA]" },
				{ new Guid("ef24dabb-bd8c-438f-b870-f5f99a9f3c4e"), "ACORD 290 OH]" },
				{ new Guid("764fd2d4-e302-4331-82fd-f709ca7fef38"), "ACORD 39 NY]" },
				{ new Guid("d7008a2a-5842-4488-a313-f88eba93cbf0"), "ACORD 138 ID]" },
				{ new Guid("3cb5f2df-faa6-4f3e-a17e-f9be3d3cc4a6"), "ACORD 144]" },
				{ new Guid("1e56ea5f-3a83-422d-ba69-faea304b9c81"), "ACORD 137 PA]" },
				{ new Guid("c530f393-82cb-4b38-86dc-fb01d9f7487f"), "ACORD 159]" },
				{ new Guid("eca2b5dc-7520-40e9-bfbe-fd94d841babc"), "ACORD 90 ID]" },
				{ new Guid("2fe5b8eb-0a87-4a6f-89f2-fe9bd61eec58"), "ACORD 185]" },
				{ new Guid("6fc28c55-c2a7-4869-a8f8-feaff2fb6c5c"), "ACORD 137 NM]" },
				{ new Guid("464a1763-fa39-401b-bfff-ffb8593ec5a3"), "ACORD 139]" },
			};
		}
	}
}