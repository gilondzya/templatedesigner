﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using Appulate.Business;
using Appulate.Uplink.Templates;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Utilities.Misc;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public partial class LoadingControl {
		public const string UplinkTemplatesDirectory = @"D:\Uplink\Templates";
		public const string UplinkTemplatesResourcesDirectory = @"D:\Uplink\Resources\Templates";

		private readonly UpdateProgressBarDelegate _updatePbDelegate;
		private delegate void UpdateProgressBarDelegate(DependencyProperty dp, Object value);

		public Dictionary<string, DocumentFormatTemplate<PageFormatTemplate>> Documents { get; private set; }

		public LoadingControl() {
			InitializeComponent();
			_updatePbDelegate = progressBar.SetValue;
			Visibility = Visibility.Hidden;
		}

		public void Load() {
			Visibility = Visibility.Visible;
			progressBar.IsIndeterminate = false;
			//string storagePath = Settings.Instance.UplinkTemplatesDirectory;
			string storagePath = UplinkTemplatesDirectory;
			var documents = new Dictionary<string, DocumentFormatTemplate<PageFormatTemplate>>(800);
			if (Directory.Exists(storagePath)) {
				documents.Clear();
				string[] innerDirectories = Directory.GetDirectories(storagePath);
				progressBar.Minimum = 0;
				progressBar.Maximum = innerDirectories.Length;
				foreach (string innerDirectory in innerDirectories) {
					string filePath = Path.Combine(innerDirectory, UplinkTemplateBase.FileName);
					if (File.Exists(filePath)) {
						lFile.Content = innerDirectory;
						var documentFormatTemplate = XmlUtility.FromXmlFile<DocumentFormatTemplate<PageFormatTemplate>>(filePath);

						documentFormatTemplate.Pages.ForEach(p => { p.AllRectangles = p.AllRectangles.ToArray(); });
						documentFormatTemplate.Save(filePath);

						documents.Add(innerDirectory, documentFormatTemplate);
					}
					Dispatcher.Invoke(_updatePbDelegate, DispatcherPriority.Background, new object[] { RangeBase.ValueProperty, ++progressBar.Value });
				}
			} else {
				MyMessageBox.Error("The storage folder doesn't exists");
			}
			Documents = documents;
			Visibility = Visibility.Hidden;
		}
	}
}
