﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public static class AmsDocumentsUtils {
		private const string PositionValueGroupName = "val";
		private static readonly Regex ItemPositionRegex = new Regex("\"(?<" + PositionValueGroupName + ">\\d+)\"", RegexOptions.Compiled);

		public static int[] GetMethodCollectionPositions(string mapping) {
			MatchCollection matches = ItemPositionRegex.Matches(mapping);
			return matches.Cast<Match>().Select(x => int.Parse(x.Groups[PositionValueGroupName].Value)).ToArray();
		}

		public static string SetMethodCollectionPosition(string destination, int position) {
			return ItemPositionRegex.Replace(destination, "\"" + position + "\"");
		}
	}
}