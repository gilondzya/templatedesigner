﻿using System;
using System.Collections.Generic;
using System.IO;
using Appulate.Business;
using Appulate.Domain.Entities;
using Appulate.Uplink.Templates;
using Appulate.Uplink.Templates.Serialization;
using Appulate.Uplink.TemplatesDesigner.Configuration;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.Utils;
using Appulate.Utilities;
using Appulate.Utilities.Misc;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class MyDocument {
		private readonly List<MyPage> _pages;
		private int _pageIndex;

		public MetaInformation MetaInfo { get; set; }
		public UplinkTemplateProperties Properties { get; set; }
		public MyCanvas Canvas { get; private set; }
		public MyPage CurrentPage => _pages.Count == 0 ? null : _pages[CurrentPageIndex];
		public string FileName { get; private set; }
		public bool IsModified { get; set; }

		public MyDocument(MyCanvas canvas) {
			IsModified = false;
			Canvas = canvas;
			MetaInfo = new MetaInformation();
			_pages = new List<MyPage> { new MyPage(this) };
			CurrentPageIndex = 0;
		}

		public void AddPage() {
			_pages.Add(new MyPage(this));
			IsModified = true;
		}

		public int CurrentPageIndex {
			get { return _pageIndex; }
			set {
				if (_pageIndex == value) {
					return;
				}
				CurrentPageChanging(this, EventArgs.Empty);
				_pageIndex = value;
				CurrentPageChanged(this, EventArgs.Empty);
			}
		}

		public int PageCount => _pages.Count;

		public event EventHandler CurrentPageChanging;
		public event EventHandler CurrentPageChanged;

		public void Save(string fileName, bool updateOcr) {
			EnsureThat.IsNotNullOrEmpty(fileName, "File name is empty.");
			if (Properties.UplinkAmsList.Count == 0) {
				throw new Exception("Ams list is empty.\nIt should contain 'Unknown' Ams at least");
			}
			FileName = fileName;
			var documentFormat = new DocumentFormatTemplate<PageFormatTemplate> { MetaInformation = MetaInfo, Properties = Properties};
			
			for (int i = 0; i < _pages.Count; i++) {
				try {
					_pages[i].IsMultiuse = i != 0 || Properties.Type != UplinkTemplateType.Acord;
					documentFormat.Pages.Add(_pages[i].GetPageFormat());
				} catch (InvalidMappingException ex) {
					throw new InvalidMappingException($"{ex.Message}\r\nPage number: {i + 1}");
				}
			}
			documentFormat.Save(fileName);
			var fInfo = new FileInfo(fileName);
			if (updateOcr) {
				string resourcesFolder = fInfo.Directory.FullName.Replace(LoadingControl.UplinkTemplatesDirectory, LoadingControl.UplinkTemplatesResourcesDirectory);
				OcrUtils.CreateOcrTemplate(fInfo.Directory.FullName, resourcesFolder, documentFormat);
			}
			IsModified = false;
		}

		public void Save() {
			Save(FileName, false);
		}

		public void Load(string fileName) {
			FileName = fileName;
			IsModified = false;
			var documentDocumentFormatTemplate = XmlUtility.FromXmlFile<DocumentFormatTemplate<PageFormatTemplate>>(fileName);
			MetaInfo = documentDocumentFormatTemplate.MetaInformation;
			Properties = documentDocumentFormatTemplate.Properties;
			_pages.Clear();
			_pageIndex = int.MinValue;
			foreach (PageFormatTemplate pageFormat in documentDocumentFormatTemplate.Pages) {
				var page = new MyPage(this);
				page.Load(pageFormat);
				_pages.Add(page);
			}
		}

		public void RemoveCurrentPage() {
			CurrentPageChanging(this, EventArgs.Empty);
			_pages.RemoveAt(_pageIndex);
			IsModified = true;
			if (_pageIndex >= _pages.Count) {
				_pageIndex--;
			}
			if (_pages.Count == 0) {
				AddPage();
				_pageIndex++;
			}
			CurrentPageChanged(this, EventArgs.Empty);
		}

		public string GetImagesFolder() {
			string folderId = Path.GetFileName(FileName.Replace("\\format.xml", string.Empty));
			return Path.Combine(LoadingControl.UplinkTemplatesResourcesDirectory, folderId);
		}
	}
}