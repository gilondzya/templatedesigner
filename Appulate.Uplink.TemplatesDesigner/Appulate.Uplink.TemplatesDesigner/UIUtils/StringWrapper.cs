﻿namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class StringWrapper {
		public StringWrapper() {}

		public StringWrapper(string value) {
			Value = value;
		}

		public string Value { get; set; }
	}
}