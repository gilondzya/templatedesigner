﻿using System;
using System.Collections.Generic;
using System.Linq;
using Appulate.Cache;
using Appulate.DataAccess.Forms;
using Appulate.Domain.Entities;
using Appulate.Domain.Entities.Supplementary;
using Appulate.Domain.Repository;

namespace Appulate.Uplink.TemplatesDesigner.Repository {
	public class DocumentDesignerTemplateRepository {
		private readonly ITemplateRepository _templateRepository;
		private readonly IFormQuestionsRetriever _formQuestionsRetriever;
		private readonly IGenericQuestionsRetriever _questionsRetriever;
		private readonly ITemplatesRetriever _templatesRetriever;

		public DocumentDesignerTemplateRepository(ITemplateRepository templateRepository, IGenericQuestionsRetriever questionsRetriever, IFormQuestionsRetriever formQuestionsRetriever, ITemplatesRetriever templatesRetriever) {
			_templateRepository = templateRepository;
			_formQuestionsRetriever = formQuestionsRetriever;
			_questionsRetriever = questionsRetriever;
			_templatesRetriever = templatesRetriever;
		}

		public Dictionary<Guid, string> GetAllTemplatesForUplinkDocuments() {
			return _templateRepository.Get(
				t => t.Enabled && (t.Type == TemplateType.Acord || t.UplinkDocuments.Any()),
				t => new { t.Id, t.Name }
			).ToDictionary(x => x.Id, x => x.Name);
		}

		public bool MappingConstraintsCheck(string mappingText, Guid templateId) {
			QuestionDescription control = _questionsRetriever.GetByName(mappingText);
			if (control == null) {
				return true;
			}
			IEnumerable<Guid> versionIds = _templatesRetriever.TemplateVersions.Where(v => v.TemplateId == templateId).Select(v => v.Id);
			return versionIds.All(id => _formQuestionsRetriever.GetAllParentsOfQuestion(control.Id, id).All(p => p.ControlType != ControlType.Collection));
		}
	}
}