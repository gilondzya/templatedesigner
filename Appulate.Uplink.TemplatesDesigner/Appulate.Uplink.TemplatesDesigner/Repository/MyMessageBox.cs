using System;
using System.Windows;

namespace Appulate.Uplink.TemplatesDesigner.Repository {
	public static class MyMessageBox {
		private const string ApplicationName = "Uplink Document Designer";

		public static void Warning(string text) {
			MessageBox.Show(text, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
		}

		public static MessageBoxResult Question(string text) {
			return MessageBox.Show(text, ApplicationName, MessageBoxButton.OKCancel, MessageBoxImage.Question);
		}

		public static MessageBoxResult Exclaim(string text) {
			return MessageBox.Show(text, ApplicationName, MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
		}

		public static void SimpleMessage(string text) {
			MessageBox.Show(text, ApplicationName, MessageBoxButton.OK, MessageBoxImage.Information);
		}

		public static void Error(string text, Exception innerException = null) {
			string message = innerException == null ? text : $"{text}\n\nInner exception:\n{innerException.Message}";
			MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
		}
	}
}