﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Appulate.Domain.Entities;
using Appulate.Uplink.Consts;
using Appulate.Uplink.Templates;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.Templates.Serialization;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Appulate.Utilities;
using Appulate.Utilities.Misc;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	public partial class WindowSearch {
		private Dictionary<string, DocumentFormatTemplate<PageFormatTemplate>> _documents;
		private readonly List<DocumentFormatTemplate<PageFormatTemplate>> _foundResultDocuments = new List<DocumentFormatTemplate<PageFormatTemplate>>();
		private readonly Dictionary<PageFormatTemplate, List<MappingRectangle>> _foundResult = new Dictionary<PageFormatTemplate, List<MappingRectangle>>();
		private Regex _foundFilter;

		public Dictionary<string, DocumentFormatTemplate<PageFormatTemplate>> Documents {
			private get {
				EnsureThat.IsNotNull(_documents, "Documents are null");
				return _documents;
			}
			set { _documents = value; }
		}
		private enum SearchType {
			PartialMatch = 0,
			FullMatch = 1,
			RegularExpression = 2
		}

		public WindowSearch() {
			InitializeComponent();
			cbInputType.ItemsSource = EnumUtils.All<InputType>();
			cbSearchType.ItemsSource = EnumUtils.All<SearchType>().Select(st => st.GetFriendlyName());
			cbReplaceMappingMode.ItemsSource = cbSearchMappingMode.ItemsSource = EnumUtils.All<MappingMode>();
			cbConverter.Items.Add(string.Empty);
			TextParsing.ValueConverters.ValueConvertersFactory.GetValueConverters().Select(c => c.Key).ToList().ForEach(k => cbConverter.Items.Add(k));

			cbInputType.SelectedIndex = 0;
			cbSearchType.SelectedIndex = 0;
			cbReplaceMappingMode.SelectedIndex = 0;
			cbSearchMappingMode.SelectedIndex = 0;
			cbConverter.SelectedIndex = 0;
		}

		#region Search
		private void btnSearch_Click(object sender, RoutedEventArgs e) {
			tbxMappings.Text = tbxResult.Text = string.Empty;
			_foundResultDocuments.Clear();
			_foundResult.Clear();
			_foundFilter = null;
			string searchText = tbSearch.Text;
			var curSearchType = (SearchType) cbSearchType.SelectedIndex;
			MappingMode? mappingMode = cbxSearchMappingMode.IsChecked.Value ? (MappingMode) cbSearchMappingMode.SelectedValue : (MappingMode?)null;
			if (searchText.IsEmpty()) {
				return;
			}
			if (curSearchType == SearchType.RegularExpression) {
				try {
					_foundFilter = new Regex(searchText,RegexOptions.Compiled | RegexOptions.IgnoreCase);
				} catch {
					MyMessageBox.Error("Failed to create a regular expression filter. Check expression.");
					return;
				}
			}

			foreach (DocumentFormatTemplate<PageFormatTemplate> curDocument in Documents.Values) {
				bool found = false;
				foreach (PageFormatTemplate curPage in curDocument.Pages) {
					List<MappingRectangle> foundRectangles = curPage.AllRectangles.Where(r => CheckMatch(searchText, r, curSearchType, mappingMode)).ToList();
					if (foundRectangles.Count > 0) {
						_foundResult.Add(curPage, foundRectangles);
						found = true;
					}
				}
				if (found) {
					_foundResultDocuments.Add(curDocument);
					string docInfo = $"({curDocument.Properties.Name} {curDocument.Properties.State}) {curDocument.Properties.SyncId}";
					tbxResult.Text = curDocument.Properties.Type == UplinkTemplateType.Attachment
						? tbxResult.Text.Insert(0, docInfo + " Attachment" + Environment.NewLine)
						: tbxResult.Text + docInfo + Environment.NewLine;
				}
			}
			string[] uniqueMapping = _foundResult.SelectMany(x => x.Value.Select(mr => mr.Text)).Distinct().OrderBy(s => s).ToArray();
			tbxMappings.Text = string.Concat(uniqueMapping.SelectMany(text => text + "\n"));

			lblResultUniqueMapping.Content = $"{uniqueMapping.Length} mapping found.";
			lblResultSummary.Content = $"{_foundResultDocuments.Count} documents found.";
		}

		private bool CheckMatch(string searchText, MappingRectangle rec, SearchType curSearchType, MappingMode? mappingMode) {
			if ((mappingMode.HasValue && mappingMode != rec.Mode) || string.IsNullOrEmpty(rec.Text)) {
				return false;
			}
			switch (curSearchType) {
				case SearchType.PartialMatch: {
					return rec.Text.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) > -1;
				}
				case SearchType.FullMatch: {
					return rec.Text.Equals(searchText, StringComparison.OrdinalIgnoreCase);
				}
				case SearchType.RegularExpression: {
					return _foundFilter.IsMatch(rec.Text);
				}
			}
			return false;
		}
		#endregion

		#region Replace
		private void btnReplace_Click(object sender, RoutedEventArgs e) {
			if (_foundResultDocuments.Count == 0) {
				return;
			}
			bool changeText = chbReplaceText.IsChecked.Value;
			string replaceText = changeText ? tbxReplacement.Text : null;
			string searchText = tbSearch.Text;
			bool removeMappingRectangles = string.IsNullOrEmpty(replaceText) && changeText;
			if (removeMappingRectangles && MyMessageBox.Question("\"Replace Text\" is empty. Do you want to delete the found MappingTexts?") == MessageBoxResult.Cancel) {
				return;
			}
			if (!string.IsNullOrEmpty(replaceText) && MyMessageBox.Question($"Do you want to replace the found MappingTexts with:\n {replaceText}?") == MessageBoxResult.Cancel) {
				return;
			}
			if (!changeText && MyMessageBox.Question("Do you want to change parameters of found MappingTexts?") == MessageBoxResult.Cancel) {
				return;
			}

			bool replaceOnlySearchPart = cbxOnlySearchPart.IsChecked.Value;
			bool changeAssumePositions = cbxAssumePositions.IsChecked.Value;
			bool copyRegion = cbxCopyRegion.IsChecked.Value;
			int? selectedInputType = chbxInputType.IsChecked.Value ? (int)((InputType) cbInputType.SelectedValue) : (int?)null;
			MappingMode? selectedMappingMode = chbxReplaceMappingMode.IsChecked.Value ? (MappingMode)cbReplaceMappingMode.SelectedValue : (MappingMode?)null;
			string converter = cbxConverter.IsChecked.Value ? cbConverter.Text : null;

			foreach (KeyValuePair<PageFormatTemplate, List<MappingRectangle>> pair in _foundResult) {
				PageFormatTemplate page = pair.Key;
				List<MappingRectangle> mapList = page.AllRectangles.ToList();
				foreach (MappingRectangle mappingRectangle in pair.Value) {
					if (removeMappingRectangles) {
						mapList.Remove(mappingRectangle);
						continue;
					}
					if (changeText && replaceOnlySearchPart) {
						mappingRectangle.Text = mappingRectangle.Text.ReplaceCaseInsensitive(searchText, replaceText);
						ProcessProperties(mappingRectangle, selectedInputType, selectedMappingMode, converter);
						continue;
					}
					if (copyRegion) {
						MappingRectangle newRectangle = CopyRectangle(mappingRectangle);
						TransformRectangle(newRectangle, replaceText, changeAssumePositions);
						ProcessProperties(mappingRectangle, selectedInputType, selectedMappingMode, converter);
						mapList.Add(newRectangle);
						continue;
					}
					TransformRectangle(mappingRectangle, replaceText, changeAssumePositions);
					ProcessProperties(mappingRectangle, selectedInputType, selectedMappingMode, converter);
				}
				page.AllRectangles = mapList.ToArray();
			}
			foreach (DocumentFormatTemplate<PageFormatTemplate> doc in _foundResultDocuments) {
				doc.Save(Path.Combine(_documents.First(x => x.Value == doc).Key, UplinkFiles.FormatFile));
			}
			MyMessageBox.SimpleMessage(_foundResultDocuments.Count + " documents successfully saved");
			e.Handled = true;
		}

		private MappingRectangle CopyRectangle(MappingRectangle rectangle) {
			return new MappingRectangle {
				BottomRight = rectangle.BottomRight,
				Id = Guid.NewGuid(),
				InputType = rectangle.InputType,
				Mode = rectangle.Mode,
				Text = rectangle.Text,
				TopLeft = rectangle.TopLeft
			};
		}

		private void TransformRectangle(MappingRectangle mappingRectangle, string replaceText, bool changeAssumePositions) {
			if (!string.IsNullOrEmpty(replaceText)) {
				if (changeAssumePositions) {
					int[] positions = AmsDocumentsUtils.GetMethodCollectionPositions(mappingRectangle.Text);
					mappingRectangle.Text = positions.Length > 0 ? AmsDocumentsUtils.SetMethodCollectionPosition(replaceText, positions[0]) : replaceText;
				} else {
					mappingRectangle.Text = replaceText;
				}
			}
		}

		private void ProcessProperties(MappingRectangle mappingRectangle, int? selectedInputType, MappingMode? selectedMappingMode, string converter) {
			if (selectedInputType.HasValue) {
				mappingRectangle.InputType = selectedInputType.Value;
			}
			if (selectedMappingMode.HasValue) {
				mappingRectangle.Mode = selectedMappingMode.Value;
			}
		}
		#endregion

		#region CheckBoxCheckedEvent
		private void chbxInputType_CheckedChanged(object sender, RoutedEventArgs e) {
			cbInputType.IsEnabled = chbxInputType.IsChecked.Value;
			e.Handled = true;
		}

		private void cbxSearchMappingMode_CheckedChanged(object sender, RoutedEventArgs e) {
			cbSearchMappingMode.IsEnabled = cbxSearchMappingMode.IsChecked.Value;
			e.Handled = true;
		}

		private void chbxReplaceMappingMode_Checked(object sender, RoutedEventArgs e) {
			cbReplaceMappingMode.IsEnabled = chbxReplaceMappingMode.IsChecked.Value;
			e.Handled = true;
		}

		private void cbxConverter_Checked(object sender, RoutedEventArgs e) {
			cbConverter.IsEnabled = cbxConverter.IsChecked.Value;
			e.Handled = true;
		}

		private void chbReplaceText_CheckedChanged(object sender, RoutedEventArgs e) {
			tbxReplacement.IsEnabled = cbxAssumePositions.IsEnabled = chbReplaceText.IsChecked.Value;
			e.Handled = true;
		}
		#endregion
	}
}
