﻿using System;
using System.Globalization;
using System.Windows.Data;
using Appulate.Domain.Entities.Supplementary;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	[ValueConversion(typeof (string), typeof (string))]
	public class StatusConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			return ((UplinkTemplateStatus)System.Convert.ToInt32(value)).ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return Enum.Parse(typeof(UplinkTemplateStatus), value.ToString());
		}
	}
}
