﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Appulate.Uplink.TextParsing.Utils.ParserResources;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public static class StringExtensions {
		public static IEnumerable ConvertToBindableList<T>(this IEnumerable<T> source) {
			if (typeof (T) == typeof (string)) {
				return source.Cast<string>().Select(x => new StringWrapper(x)).ToList();
			}
			if (typeof (T) == typeof (AcordPropertiesInfo)) {
				return source.Cast<AcordPropertiesInfo>().OrderBy(i => i.AcordName).Select(x => new AcordViewData(x)).ToList();
			}
			return new ObservableCollection<T>(source);
		}
	}
}
