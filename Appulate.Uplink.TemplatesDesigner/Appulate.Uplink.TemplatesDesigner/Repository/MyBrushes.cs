﻿using System.Windows;
using System.Windows.Media;

namespace Appulate.Uplink.TemplatesDesigner.Repository {
	public static class MyBrushes {
		private const byte AsIsAlpha = 130;

		private static readonly Color[] MyColors = new[] {
			Color.FromArgb(AsIsAlpha, 191, 214, 245), Color.FromArgb(AsIsAlpha, 191, 245, 214),
			Color.FromArgb(AsIsAlpha, 245, 214, 191), Color.FromArgb(AsIsAlpha, 245, 191, 214),
			Color.FromArgb(AsIsAlpha, 214, 191, 245), Color.FromArgb(AsIsAlpha, 214, 245, 191)
		};

		private static readonly Color WhiteColor = Color.FromArgb(AsIsAlpha, 255, 255, 255);

		static MyBrushes() {
			Stroke = Brushes.Gray;
			HoverStroke = Brushes.Gray;
			SelectedStroke = Brushes.Red;
			HoverSelectedStroke = Brushes.Red;
			SelectorFill = new SolidColorBrush(Color.FromArgb(20, 10, 36, 106));
			SelectorStroke = new SolidColorBrush(Color.FromRgb(10, 36, 106));
			MappingText = Brushes.Black; 
			MappingTextTypeFace = new Typeface(new FontFamily("Consolas"), FontStyles.Normal, FontWeights.UltraBlack, FontStretches.Normal, new FontFamily("Courier New"));
		}

		public static Brush Stroke { get; private set; }
		public static Brush HoverStroke { get; private set; }
		public static Brush SelectedStroke { get; private set; }
		public static Brush HoverSelectedStroke { get; private set; }
		public static Brush SelectorFill { get; private set; }
		public static Brush SelectorStroke { get; private set; }
		public static Brush MappingText { get; private set; }
		public static Typeface MappingTextTypeFace { get; private set; }

		public static Brush GetBrush(int colorIndex, bool selected, bool hover) {
			if (colorIndex >= MyColors.Length) colorIndex %= MyColors.Length;
			Color startColor = MyColors[colorIndex];
			int r = startColor.R;
			int g = startColor.G;
			int b = startColor.B;
			if (selected) {
				if (hover) {
					r -= 20;
					g -= 20;
					b -= 20;
				} else {
					r -= 40;
					g -= 40;
					b -= 40;
				}
			} else {
				if (hover) {
					r += 20;
					g += 20;
					b += 20;
				}
			}
			startColor.R = (byte) (r <= byte.MaxValue ? r : byte.MaxValue);
			startColor.G = (byte) (g <= byte.MaxValue ? g : byte.MaxValue);
			startColor.B = (byte) (b <= byte.MaxValue ? b : byte.MaxValue);
			return new LinearGradientBrush(startColor, WhiteColor, 90.0);
		}
	}
}