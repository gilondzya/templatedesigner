﻿namespace Appulate.Uplink.TemplatesDesigner.UIUtils {

	public class StateInfoComboBoxItem {
		public string StateId { get; set; }
		public string StateName { get; set; }

		public override string ToString() {
			return StateName;
		}
	}
}
