﻿namespace Appulate.Uplink.TemplatesDesigner.Repository {
	public static class MyZIndices {
		public const int RectangularSelector = 100;
		public const int BackgroundPicture = 10;
		public const int SelectedRectangle = 30;
		public const int Rectangle = 20;
	}
}
