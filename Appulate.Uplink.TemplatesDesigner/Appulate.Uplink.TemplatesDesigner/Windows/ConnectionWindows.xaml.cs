﻿using System.Configuration;
using System.Windows;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	/// <summary>
	/// Interaction logic for SetConnection.xaml
	/// </summary>
	public partial class ConnectionWindows : Window {

		public string ConnectionString => tb_connectionString.Text;

		public ConnectionWindows() {
			InitializeComponent();
			tb_connectionString.Text = ConfigurationManager.AppSettings["ConnectionString"];
		}

		private void button_Click(object sender, RoutedEventArgs e) {
			ConfigurationManager.AppSettings["ConnectionString"] = tb_connectionString.Text;
			DialogResult = true;
			Close();
		}
	}
}
