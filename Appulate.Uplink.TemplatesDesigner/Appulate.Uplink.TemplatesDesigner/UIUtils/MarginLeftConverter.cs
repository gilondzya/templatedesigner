﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	[ValueConversion(typeof(double), typeof(double))]
	public class MarginLeftConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			return new Thickness(((double)value - System.Convert.ToInt32(parameter))/2, 0, 0, 0);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return DependencyProperty.UnsetValue;
		}
	}
}