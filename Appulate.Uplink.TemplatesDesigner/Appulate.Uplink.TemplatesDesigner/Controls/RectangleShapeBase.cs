﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using CanvasControl = System.Windows.Controls.Canvas;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public abstract class RectangleShapeBase : Shape {
		protected abstract MyCanvas Canvas { get; }

		public double Left {
			get { return CanvasControl.GetLeft(this); }
			set { CanvasControl.SetLeft(this, value); }
		}

		public double Top {
			get { return CanvasControl.GetTop(this); }
			set { CanvasControl.SetTop(this, value); }
		}

		public int ZIndex {
			get { return Panel.GetZIndex(this); }
			set { Panel.SetZIndex(this, value); }
		}

		protected override Geometry DefiningGeometry {
			get { return new RectangleGeometry(new Rect(0, 0, Width, Height)); }
		}

		public void Resize(double offsetX, double offsetY) {
			if (Width + offsetX > MinWidth) {
				Width += offsetX;
			}
			if (Height + offsetY > MinHeight) {
				Height += offsetY;
			}
		}

		public void Resize(Point from, Point to) {
			Resize(to.X - from.X, to.Y - from.Y);
		}
	}
}
