﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Appulate.Uplink.TemplatesDesigner.Utils;
using Appulate.Uplink.TextParsing.Utils.ParserResources;
using Appulate.Utilities.State;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public partial class AcordPropertiesControl : UserControl {

		private readonly AcordPropertiesInfoList _acordPropertiesList;
		private readonly Regex AcordNameRegex = new Regex(@"^Acord\s+\d+", RegexOptions.Compiled | RegexOptions.IgnoreCase);

		public AcordPropertiesControl() {
			InitializeComponent();
			_acordPropertiesList = new AcordPropertiesInfoList();
			_acordPropertiesList.Load();

			cbAcordList.ItemsSource = _acordPropertiesList.ResourcesList.ConvertToBindableList();
			cbDefInsurance.ItemsSource = AcordViewData.InsurenceTypes;
			cbResolInsurance.ItemsSource = AcordViewData.InsurenceTypes;
			cbExtraCovLine.ItemsSource = AcordViewData.InsurenceTypes;
			cbAcordList.SelectedIndex = 0;
		}

		public void SaveIfHasChanges(AcordViewData data = null) {
			data = data ?? (AcordViewData)cbAcordList.SelectedItem;
			if (HasChanges(data) && MessageBox.Show("Save changes?", string.Empty, MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
				SaveAcord(data);
			}
			ReloadAcord();
		}

		private void ReloadAcord() {
			var data = (AcordViewData)cbAcordList.SelectedItem;
			if (data != null) {
				cbDefInsurance.SelectedItem = data.DefaultInsuranceTypeSelected;
				cbResolInsurance.SelectedItem = data.ResolveInsuranceTypeSelected;
				cbExtraCovLine.SelectedItem = data.ExtraCoverageLineSelected;
				listTitleWithState.ItemsSource = data.Item.TitleList.Select(ti => new TitleViewData(ti)).ToList();
			}
		}

		private void SaveAcord(AcordViewData data) {
			data.Item.DefaultInsuranceType = ((KeyValuePair<Guid, string>)cbDefInsurance.SelectedItem).Key;
			data.Item.ResolveInsuranceType = ((KeyValuePair<Guid, string>)cbResolInsurance.SelectedItem).Key;
			data.Item.ExtraCoverageLine = ((KeyValuePair<Guid, string>)cbExtraCovLine.SelectedItem).Key;
			data.Item.TitleList = ((List<TitleViewData>)listTitleWithState.ItemsSource).Select(i => i.Item).ToList();

			List<AcordPropertiesInfo> source = _acordPropertiesList.ResourcesList.ToList();
			source.ForEach(i => i.TitleList.ForEach(ti => ti.Texts.RemoveAll(title => string.IsNullOrEmpty(title.Text))));
			source.ForEach(i => i.TitleList.RemoveAll(t => t.Texts.Count == 0));
			_acordPropertiesList.ResourcesList = source;
			ParserResoursesUtil.TrySaveParserResource(_acordPropertiesList, showMessage: false);
		}

		private bool HasChanges(AcordViewData data) {
			bool hasChanges = ((KeyValuePair<Guid, string>)cbDefInsurance.SelectedItem).Key != data.DefaultInsuranceTypeSelected.Key ||
			                  ((KeyValuePair<Guid, string>)cbResolInsurance.SelectedItem).Key != data.ResolveInsuranceTypeSelected.Key ||
			                  ((KeyValuePair<Guid, string>)cbExtraCovLine.SelectedItem).Key != data.ExtraCoverageLineSelected.Key;

			if (!hasChanges) {
				var source = (List<TitleViewData>)listTitleWithState.ItemsSource;
				if (source.Count == data.Item.TitleList.Count) {
					foreach (TitleInfo sourceInfo in source.Select(i => i.Item)) {
						TitleInfo sameGroup = data.Item.TitleList.FirstOrDefault(t => t.State == sourceInfo.State);
						if (sameGroup == null || sameGroup.Texts.Count != sourceInfo.Texts.Count) {
							return true;
						}
						if (sourceInfo.Texts.Any(title => sameGroup.Texts.All(t => t.Text != title.Text))) {
							return true;
						}
					}
				} else {
					return true;
				}
			}
			return hasChanges;
		}

		private void AddAcord(string name) {
			List<AcordPropertiesInfo> source = _acordPropertiesList.ResourcesList.ToList();
			var newAcord = new AcordPropertiesInfo { AcordName = name };
			source.Add(newAcord);
			UpdateSource(source);
			cbAcordList.SelectedItem = ((List<AcordViewData>)cbAcordList.ItemsSource).First(i => i.Item == newAcord);
		}

		private void DeleteAcord() {
			if (MessageBox.Show("Delete current Acord?", string.Empty, MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
				List<AcordPropertiesInfo> source = _acordPropertiesList.ResourcesList.ToList();
				source.Remove(((AcordViewData)cbAcordList.SelectedItem).Item);
				UpdateSource(source);
				cbAcordList.SelectedIndex = 0;
			}
		}

		private void UpdateSource(List<AcordPropertiesInfo> source) {
			_acordPropertiesList.ResourcesList = source;
			ParserResoursesUtil.TrySaveParserResource(_acordPropertiesList, showMessage: false);
			cbAcordList.ItemsSource = _acordPropertiesList.ResourcesList.ConvertToBindableList();
		}

		private void btnAcordTitlesSave_Click(object sender, RoutedEventArgs e) {
			SaveAcord((AcordViewData)cbAcordList.SelectedItem);
			ReloadAcord();
		}

		private void SelectedAcordChanged(object sender, SelectionChangedEventArgs e) {
			if (e.RemovedItems.Count > 0) {
				SaveIfHasChanges((AcordViewData)e.RemovedItems[0]);
			} else {
				ReloadAcord();
			}
		}

		private void btnAcordDelete_Click(object sender, RoutedEventArgs e) => DeleteAcord();

		private void btnAcordTitlesAdd_Click(object sender, RoutedEventArgs e) {
			SaveIfHasChanges();
			SetVisibilityForNewAcordControl(visible: true);
		}

		private void Button_Click(object sender, RoutedEventArgs e) {
			if (AcordNameRegex.IsMatch(tbNewAcordName.Text) && StatesCache.AllIds.All(s => !tbNewAcordName.Text.EndsWith(s, StringComparison.OrdinalIgnoreCase))) {
				if (_acordPropertiesList.ResourcesList.All(a => !a.AcordName.Equals(tbNewAcordName.Text, StringComparison.InvariantCultureIgnoreCase))) {
					AddAcord(tbNewAcordName.Text);
					SetVisibilityForNewAcordControl(visible: false);
				} else {
					MessageBox.Show("Acord name already exists", string.Empty, MessageBoxButton.OK);
				}
			} else {
				MessageBox.Show("Incorrect Acord name", string.Empty, MessageBoxButton.OK);
			}
		}

		private void Button_Click_1(object sender, RoutedEventArgs e) {
			SetVisibilityForNewAcordControl(visible: false);
		}

		private void SetVisibilityForNewAcordControl(bool visible) {
			if (visible) {
				tbNewAcordName.Text = string.Empty;
				gMainAcordView.Visibility = Visibility.Collapsed;
				spAddNewControl.Visibility = Visibility.Visible;
			} else {
				gMainAcordView.Visibility = Visibility.Visible;
				spAddNewControl.Visibility = Visibility.Collapsed;
			}
		}

		private void AddTitle_Click(object sender, RoutedEventArgs e) {
			List<TitleViewData> source = ((List<TitleViewData>)listTitleWithState.ItemsSource).ToList();
			source.Add(new TitleViewData(new TitleInfo()));
			listTitleWithState.ItemsSource = source;
		}

		private void RemoveTitle_Click(object sender, RoutedEventArgs e) {
			var selectedInfo = (TitleViewData)listTitleWithState.SelectedItem;
			if (selectedInfo != null) {
				List<TitleViewData> source = ((List<TitleViewData>)listTitleWithState.ItemsSource).ToList();
				source.Remove(selectedInfo);
				listTitleWithState.ItemsSource = source;
			}
		}
	}
}
