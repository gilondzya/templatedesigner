﻿using System;
using System.Windows.Markup;
using Appulate.Utilities;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	[MarkupExtensionReturnType(typeof (object[]))]
	public class EnumValuesGetter : MarkupExtension {
		private readonly Type _enumType;

		public EnumValuesGetter(Type enumType) {
			_enumType = enumType;
		}

		public override object ProvideValue(IServiceProvider serviceProvider) {
			EnsureThat.IsNotNull(_enumType, "The enum type is not set");

			return Enum.GetValues(_enumType);
		}
	}
}
