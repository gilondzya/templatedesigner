﻿using System;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class TemplateInfoComboBoxItem {
		public string TemplateName { get; set; }
		public string TemplateNameWithoutState { get; set; }
		public Guid TemplateId { get; set; }

		public TemplateInfoComboBoxItem(string name, Guid id) {
			TemplateName = name;
			TemplateId = id;
		}

		public override string ToString() {
			return TemplateName;
		}
	}
}