﻿using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using CanvasControl = System.Windows.Controls.Canvas;

namespace Appulate.Uplink.TemplatesDesigner.Controls
{
	public class MyImage: Image {
		private readonly Border _border;
		private readonly MyCanvas _canvas;
		private readonly FormattedText _emptyText;

		public MyImage(MyCanvas canvas) {
			_canvas = canvas;
			Stretch = Stretch.Uniform;
			_border = new Border { BorderThickness = new Thickness(1, 0, 1, 1), BorderBrush = Brushes.Gray, Child = this };
			_canvas.Children.Add(_border);
			_emptyText = new FormattedText(
				"No Development Picture",
				CultureInfo.CurrentCulture,
				FlowDirection,
				new Typeface(new FontFamily("Verdana"),
					FontStyles.Normal,
					FontWeights.UltraBold,
					FontStretches.SemiExpanded,
					new FontFamily("Arial")),
				32.0,
				new LinearGradientBrush(Colors.Gray, Colors.LightBlue, 90.0)
			);
		}

		protected override void OnRender(DrawingContext dc) {
			base.OnRender(dc);
			if (Source != null) return;

			dc.DrawText(_emptyText, new Point(10.0, 10.0));
		}

		public double LeftMargin {
			get { return _border.Margin.Left + _border.BorderThickness.Left; }
		}
		
		public void Load(BitmapImage bitmapImage) {
			try {
				Width = bitmapImage.Width;
				Height = bitmapImage.Height;
				if (bitmapImage.Width > _canvas.ActualWidth) {
					_canvas.Width = bitmapImage.Width;
				}
				if (bitmapImage.Height > _canvas.ActualHeight) {
					_canvas.Height = bitmapImage.Height;
				}
				Source = bitmapImage;
				var binding = new Binding("ActualWidth") {
					Source = _canvas,
					Converter = new MarginLeftConverter(),
					ConverterParameter = Source.Width
				};
				_border.SetBinding(MarginProperty, binding);
			} catch {
				Source = null;
				CanvasControl.SetTop(this, .0);
				CanvasControl.SetLeft(this, .0);
				BindingOperations.ClearBinding(_border, MarginProperty);
			}
		}
	}
}
