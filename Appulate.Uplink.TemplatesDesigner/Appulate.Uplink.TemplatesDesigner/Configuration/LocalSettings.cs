﻿using System;
using System.Configuration;
using Appulate.Business;
using Appulate.Configuration;
using Appulate.Infrastructure;
using Appulate.Infrastructure.Modules;
using Appulate.Infrastructure.Persistence.EF.Modules;
using Appulate.Infrastructure.Settings;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Autofac;

namespace Appulate.Uplink.TemplatesDesigner.Configuration {
	public class LocalSettings : CommonSettings {
		public static LocalSettings Instance { get; } = new LocalSettings();

		private LocalSettings() { }

		public override string ConnectionString => ConfigurationManager.AppSettings["ConnectionString"];

		public override string StorageFolder => ConfigurationManager.AppSettings["StorageFolder"];

		public override TimeSpan MarketPortalSlidingExpiration => TimeSpan.FromMinutes(double.Parse(AppSettings["MarketPortalSlidingExpirationMins"]));

		public override void Initialize() {
			var builder = new ContainerBuilder();
			builder.RegisterAppulateModules();
			builder.RegisterModule<PersistanceRegistrationModule>();
			builder.RegisterModule<DbUpdateHandlerRegistrationModule>();
			builder.RegisterType<DatabaseSettings>().As<IDatabaseSettings>();
			builder.RegisterType<DocumentDesignerTemplateRepository>();
			Factory.InitComponentsRegistry(new ComponentsRegistry(builder.Build()));
			Settings.Initialize(this);
		}
	}
}