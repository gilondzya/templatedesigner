﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Appulate.Uplink.TemplatesDesigner.Utils {
	public class MappingsCheckResult {
		public MappingsCheckResult(IEnumerable<TextBlock> report, bool isRecognized) {
			Report = report.ToArray();
			IsDocumentRecognized = isRecognized;
		}

		public bool IsDocumentRecognized { get; private set; }
		public TextBlock[] Report { get; private set; }
	}
}