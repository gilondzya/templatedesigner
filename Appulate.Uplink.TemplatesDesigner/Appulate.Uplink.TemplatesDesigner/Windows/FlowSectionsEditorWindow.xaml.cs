﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.TemplatesDesigner.Controls;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TextParsing.FlowSections;
using Settings = Appulate.Business.Settings;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	public partial class FlowSectionsEditorWindow {
		private readonly RecognitionTablesList _tablesList;
		private static readonly string TablesListFilePath = System.IO.Path.Combine(LoadingControl.UplinkTemplatesDirectory, Consts.UplinkFiles.FlowSectionsFile);

		public FlowSectionsEditorWindow() {
			InitializeComponent();

			try {
				_tablesList = RecognitionTablesList.Load(TablesListFilePath);
			} catch (Exception ex) {
				MyMessageBox.Error(ex.Message);
				return;
			}
			foreach (RecognitionTable table in _tablesList.Tables.OrderBy(x => x.HeaderText)) {
				var tableExpander = new FlowSectionsTableExpander(stpTables, _tablesList) { DataContext = table };
				stpTables.Children.Insert(stpTables.Children.Count - 2, tableExpander);
			}
		}

		private void btnAdd_Click(object sender, RoutedEventArgs e) {
			var table = new RecognitionTable();
			_tablesList.Tables.Add(table);
			var tableExpander = new FlowSectionsTableExpander(stpTables, _tablesList) { DataContext = table };
			(tableExpander.Content as Expander).IsExpanded = true;
			stpTables.Children.Insert(stpTables.Children.Count - 2, tableExpander);
		}

		private void btnSave_Click(object sender, RoutedEventArgs e) {
			foreach (RecognitionTable table in _tablesList.Tables) {
				if (string.IsNullOrEmpty(table.HeaderText)) {
					MyMessageBox.Error("Table header can't be empty");
					return;
				}
				if (table.Columns.Any(x => string.IsNullOrEmpty(x.ColumnHeader))) {
					MyMessageBox.Error(string.Format("Table '{0}'\nEmpty column headers are not allowed", table.HeaderText));
					return;
				}
				if (table.Columns.Any(x => x.MappingMode == MappingMode.Checkpoint)) {
					MyMessageBox.Error(string.Format("Table '{0}'\nMapping mode 'Checkpoint' is not allowed", table.HeaderText));
					return;
				}
				if (table.Columns.Select(x => x.ColumnHeader).Distinct().Count() < table.Columns.Count) {
					MyMessageBox.Error(string.Format("Table '{0}'\nTable can't contain columns with same headers", table.HeaderText));
					return;
				}
				if (table.Columns.Any(x => x.IsMultiline) && !table.Columns.Any(x => x.IsPrimary)) {
					MyMessageBox.Error(string.Format("Table '{0}'\nTable with multiline columns must contain primary column(s)", table.HeaderText));
					return;
				}
			}
			try {
				_tablesList.Save(TablesListFilePath);
				MyMessageBox.SimpleMessage("Successfully saved in " + TablesListFilePath);
			} catch (Exception ex) {
				MyMessageBox.Error(ex.Message);
			}
		}
	}
}
