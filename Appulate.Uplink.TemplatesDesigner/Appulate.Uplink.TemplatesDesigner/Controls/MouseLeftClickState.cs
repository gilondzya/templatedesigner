﻿using System.Windows;
using System.Windows.Input;

namespace Appulate.Uplink.TemplatesDesigner.Controls
{
	public class MouseLeftClickState
	{
		public bool IsFirstMove { get; set; }
		public Point PressedAt { get; private set; }
		public MouseButtonState State { get; private set; }
		public MyRectangle Rectangle { get; set; }
		public bool RectangleClicked
		{
			get { return Rectangle != null; }
		}
		public bool CtrlPressed { get; private set; }

		public MouseLeftClickState()
		{
			Release();
		}

		public void Press(Point at, bool isCtrl)
		{
			PressedAt = at;
			State = MouseButtonState.Pressed;
			CtrlPressed = isCtrl;
		}
		public void Release()
		{
			State = MouseButtonState.Released;
			IsFirstMove = true;
			Rectangle = null;
		}
	}
}