﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Appulate.Uplink.Consts;
using Appulate.Uplink.Templates.Serialization.Ams;
using Appulate.Uplink.TemplatesDesigner.Controls;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Settings = Appulate.Business.Settings;

namespace Appulate.Uplink.TemplatesDesigner.Windows {

	public partial class AmsEditorWindow {
		public readonly string AmsListFilePath = System.IO.Path.Combine(LoadingControl.UplinkTemplatesDirectory, UplinkFiles.SupportedAmsFile);
		private readonly AmsList _amsList;
		private bool _hasChanges;

		public AmsEditorWindow(AmsList amsList) {
			InitializeComponent();
			_amsList = amsList;
			dgAmsList.ItemsSource = _amsList.Items.ConvertToBindableList();
		}

		private void Save() {
			try {
				_amsList.Items = (ObservableCollection<AmsListItem>)dgAmsList.ItemsSource;
				_amsList.Save(AmsListFilePath);
				_hasChanges = false;
				MyMessageBox.SimpleMessage("Successfully updated");
			} catch (Exception ex) {
				MyMessageBox.Error("Failed to change Ams list.\nDetails: " + ex);
			}
		}

		#region Helper routines

		private AmsListItem CreateAmsListFromControls() {
			if(string.IsNullOrEmpty(tbNewAmsName.Text)){
				MyMessageBox.Error("Ams Name can't be empty");
				return null;
			}
			if (string.IsNullOrEmpty(tbNewAmsId.Text)) {
				MyMessageBox.Error("Ams Id can't be empty");
				return null;
			}
			return new AmsListItem {
				Id = new Guid(tbNewAmsId.Text),
				IsVerified = chbNewAmsIsVerified.IsChecked.Value,
				Name = tbNewAmsName.Text,
				Version = tbNewAmsVersion.Text,
				IsVisible = cbVisible.IsChecked.Value
			};
		}

		private void FillAmsListControlsFromEntity(AmsListItem item) {
			tbNewAmsId.Text = item.Id.ToString();
			tbNewAmsName.Text = item.Name;
			tbNewAmsVersion.Text = item.Version;
			chbNewAmsIsVerified.IsChecked = item.IsVerified;
			cbVisible.IsChecked = item.IsVisible;
		}

		#endregion

		#region EventHandlers

		private void btGenId_Click(object sender, RoutedEventArgs e) {
			Guid newId = Guid.NewGuid();
			while (_amsList.Items.Any(x => x.Id == newId)) {
				newId = Guid.NewGuid();
			}
			tbNewAmsId.Text = newId.ToString();
			e.Handled = true;
		}

		private void btCreateOrEditAms_Click(object sender, RoutedEventArgs e) {
			AmsListItem newAmsItem = CreateAmsListFromControls();
			if (newAmsItem == null) {
				return;
			}
			var bindedList = (ObservableCollection<AmsListItem>) dgAmsList.ItemsSource;
			AmsListItem itemFromAmsList = bindedList.FirstOrDefault(x => x.Id == newAmsItem.Id);
			if (itemFromAmsList == null) {
				bindedList.Add(newAmsItem);
			} else {
				int index = bindedList.IndexOf(itemFromAmsList);
				bindedList[index] = newAmsItem;
			}
			_hasChanges = true;
			e.Handled = true;
		}

		private void btCancelSaveNewAms_Click(object sender, RoutedEventArgs e) {
			FillAmsListControlsFromEntity(new AmsListItem());
			tbNewAmsId.Text = string.Empty;
			dgAmsList.SelectedIndex = -1;
			e.Handled = true;
		}

		private void btnEdit_Click(object sender, RoutedEventArgs e) {
			if (dgAmsList.SelectedIndex >= 0) {
				var prop = (dgAmsList.Items[dgAmsList.SelectedIndex] as AmsListItem);
				FillAmsListControlsFromEntity(prop);
			}
			e.Handled = true;
		}

		private void btnSave_Click(object sender, RoutedEventArgs e) {
			Save();
			e.Handled = true;
		}

		private void btnDelete_Click(object sender, RoutedEventArgs e) {
			if (dgAmsList.SelectedIndex >= 0) {
				((ObservableCollection<AmsListItem>)dgAmsList.ItemsSource).Remove(dgAmsList.Items[dgAmsList.SelectedIndex] as AmsListItem);
				_hasChanges = true;
			}
			e.Handled = true;
		}
	
		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			if (!_hasChanges) {
				return;
			}
			MessageBoxResult result = MyMessageBox.Exclaim("Save changes?");
			if (result == MessageBoxResult.Yes) {
				Save();
			} else if(result == MessageBoxResult.Cancel){
				e.Cancel = true;
			}
		}

    #endregion


	}
}
