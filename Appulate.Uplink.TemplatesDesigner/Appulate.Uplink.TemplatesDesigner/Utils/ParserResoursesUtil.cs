﻿using System;
using System.Windows;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TextParsing.Utils.ParserResources;

namespace Appulate.Uplink.TemplatesDesigner.Utils {
	public static class ParserResoursesUtil {

		public static void TryLoadParserResource(IParserResource resource, string resourceName) {
			try {
				resource.Load();
			} catch (Exception ex) {
				MyMessageBox.Error(resourceName + "\n" + ex.Message);
			}
		}

		public static void TrySaveParserResource<T>(T resource, RoutedEventArgs eventArgs = null, bool showMessage = true) where T : IParserResource {
			try {
				resource.Save();
				if (showMessage) {
					MyMessageBox.SimpleMessage("Successfully saved");
				}
			} catch (Exception ex) {
				MyMessageBox.Error(ex.Message);
			}
			if (eventArgs != null) {
				eventArgs.Handled = true;
			}
		}

	}
}
