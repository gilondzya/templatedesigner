﻿using System;

namespace Appulate.Uplink.TemplatesDesigner.Repository {
	public class InvalidMappingException : Exception {
		private readonly string _message;

		public InvalidMappingException(string message) {
			_message = message;
		}

		public override string Message {
			get { return _message; }
		}
	}
}
