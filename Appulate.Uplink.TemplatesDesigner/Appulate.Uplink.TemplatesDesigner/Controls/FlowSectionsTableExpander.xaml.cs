﻿using System;
using System.Windows;
using System.Windows.Controls;
using Appulate.Uplink.TextParsing.FlowSections;

namespace Appulate.Uplink.TemplatesDesigner.Controls {

	public partial class FlowSectionsTableExpander {
		private readonly Panel _parent;
		private readonly RecognitionTablesList _tablesList;

		public FlowSectionsTableExpander(Panel parent, RecognitionTablesList list) {
			InitializeComponent();
			_parent = parent;
			_tablesList = list;
		}

		private void btnDelete_Click(object sender, RoutedEventArgs e) {
			_tablesList.Tables.Remove(DataContext as RecognitionTable);
			_parent.Children.Remove(this);
		}

		private void tbTaleHeader_LostFocus(object sender, RoutedEventArgs e) {
			for (int i = 0; i < _parent.Children.Count; i++) {
				var item = _parent.Children[i] as FlowSectionsTableExpander;
				if (item != null && string.Compare((item.DataContext as RecognitionTable).HeaderText, tbTaleHeader.Text, StringComparison.OrdinalIgnoreCase) >= 0) {
					int currentIndex = _parent.Children.IndexOf(this);
					_parent.Children.Remove(this);
					_parent.Children.Insert(currentIndex < i ? i - 1 : i, this);
					break;
				}
			}
		}
	}
}
