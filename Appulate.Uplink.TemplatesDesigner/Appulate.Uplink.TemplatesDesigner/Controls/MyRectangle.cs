using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.Templates.Serialization;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using InputType = Appulate.Uplink.Templates.Data.InputType;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class MyRectangle : RectangleShapeBase {
		#region Hidden properties

		private readonly MappingMode[] _mappingModesToCheck = {
			MappingMode.ExactControl, MappingMode.IsCheckbox, MappingMode.AnswerIsYes, MappingMode.AnswerIsNo
		};

		private static readonly Dictionary<MappingMode, string> MappingModeShortNames = new Dictionary<MappingMode, string> {
			{ MappingMode.AnswerIsNo, "no (Yes/No)" },
			{ MappingMode.AnswerIsYes, "yes (Yes/No)" },
			{ MappingMode.Checkpoint, "checkpoint" },
			{ MappingMode.ExactControl, "control" },
			{ MappingMode.IsCheckbox, "checkbox" },
			{ MappingMode.ParsingMethod, "method" }
		};

		private static int _brushIndexCounter;
		private bool _isSelected;
		private string _mappingText;
		private string _xmlPath;
		private MappingMode _mappingMode;
		private InputType _inputType;
		private readonly MyPage _page;
		private readonly int _brushIndex;
		private bool _colored;

		protected override MyCanvas Canvas => _page.Document.Canvas;

		#endregion
		
		public Guid Id { get; set; }
		public bool IntersectsSelector { get; set; }

		public string MappingText {
			get => _mappingText;
			set {
				_mappingText = value;
				UpdateToolTip();
			}
		}

		public string XmlPath {
			get => _xmlPath;
			set {
				_xmlPath = value;
				UpdateToolTip();
			}
		}

		public InputType InputType {
			get => _inputType;
			set {
				_inputType = value;
				UpdateToolTip();
			}
		}

		public MappingMode MappingMode {
			get => _mappingMode;
			set {
				_mappingMode = value;
				UpdateToolTip();
			}
		}

		public bool Colored {
			get => _colored;
			set {
				if (_colored != value) {
					_colored = value;
					SetFillBrush(false, false);
				}
			}
		}

		public bool IsSelected {
			get => _isSelected;
			set {
				_isSelected = value;
				if (value) {
					Stroke = MyBrushes.SelectedStroke;
					ZIndex = MyZIndices.SelectedRectangle;
				} else {
					Stroke = MyBrushes.Stroke;
					ZIndex = MyZIndices.Rectangle;
				}
				SetFillBrush(value, false);
			}
		}

		public MyRectangle(MyPage page) {
			MappingMode = MappingMode.ExactControl;
			MappingText = string.Empty;
			_page = page;

			MouseEnter += onMouseEnter;
			MouseLeave += onMouseLeave;
			
			_brushIndex = _brushIndexCounter++;
			_colored = true;
			SetFillBrush(false, false);
			Stroke = MyBrushes.Stroke;
			ZIndex = MyZIndices.Rectangle;
			StrokeThickness = 1;
			MinWidth = 4;
			MinHeight = 3;
			Width = 100;
			Height = 80;
		}

		protected override void OnRender(DrawingContext drawingContext) {
			base.OnRender(drawingContext);
			var text = new FormattedText($"{MappingModeShortNames[MappingMode]}: {MappingText}",
				CultureInfo.CurrentCulture,
				FlowDirection,
				MyBrushes.MappingTextTypeFace,
				Canvas.MappingTextFontSize,
				MyBrushes.MappingText) {
					MaxTextWidth = Width - 2
				};
			drawingContext.DrawText(text, new System.Windows.Point(2, 2));
		}

		public void Init(MappingRectangle mapping) {
			Left = mapping.TopLeft.X;
			Top = mapping.TopLeft.Y;
			Width = mapping.BottomRight.X - mapping.TopLeft.X;
			Height = mapping.BottomRight.Y - mapping.TopLeft.Y;
			MappingMode = mapping.Mode;
			MappingText = mapping.Text;
			XmlPath = mapping.XmlPath;
			InputType = (InputType)mapping.InputType;
			Id = mapping.Id;
		}

		public void InitMargingBinding() {
			var binding = new Binding("ActualWidth") {
				Source = Canvas,
				Converter = new MarginLeftConverter(),
				ConverterParameter = Canvas.ActualWidth - Canvas.BackgroundImage.LeftMargin * 2
			};
			SetBinding(MarginProperty, binding);
		}

		public void Move(double offsetX, double offsetY) {
			Left += offsetX;
			Top += offsetY;
		}

		public void Remove() {
			_page.RemoveRectangle(this);
		}

		public void ToggleHover() {
			InnerToggleHover(IntersectsSelector);
		}

		public MappingRectangle GetMappingRectangle() {
			var result = new MappingRectangle {
				Text = MappingText,
				XmlPath = XmlPath,
				Mode = MappingMode,
				InputType = (int)InputType,
				Id = Id,
				IsPostParsingMethod = false
			};
			short left = Convert.ToInt16(Left);
			short top = Convert.ToInt16(Top);
			short width = Convert.ToInt16(Width);
			short height = Convert.ToInt16(Height);
			result.TopLeft.SetPoint(left, top);
			result.BottomRight.SetPoint(Convert.ToInt16(left + width), Convert.ToInt16(top + height));
			return result;
		}

		public bool PointInside(System.Windows.Point point) {
			return point.X + Canvas.BackgroundImage.LeftMargin > Left && point.X < Left + Width && point.Y > Top && point.Y < Top + Height;
		}

		#region Helper routines

		private void InnerToggleHover(bool hover) {
			SetFillBrush(IsSelected, hover);
		}

		private void UpdateToolTip() {
			string xmlPrefix = XmlPath != null ? $"{XmlPath} : " : string.Empty;
			ToolTip = $"{xmlPrefix}[ {MappingMode}, {InputType}]: {MappingText}";
			InvalidateVisual();
		}

		private void SetFillBrush(bool isSelected, bool hover) {
			Fill = _colored ? MyBrushes.GetBrush(_brushIndex, isSelected, hover) : null;
		}

		private static void onMouseEnter(object sender, MouseEventArgs e) {
			((MyRectangle)sender).InnerToggleHover(true);
		}

		private static void onMouseLeave(object sender, MouseEventArgs e) {
			((MyRectangle)sender).InnerToggleHover(false);
		}

		#endregion
	}
}