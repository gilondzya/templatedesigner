﻿using Appulate.Uplink.TemplatesDesigner.Repository;

namespace Appulate.Uplink.TemplatesDesigner
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App {
		public App() {
			DispatcherUnhandledException += (sender, args) => MyMessageBox.Error("Application error\n\n" + args.Exception);
		}
	}
}
