﻿using System.Collections.Generic;
using System.Linq;
using Appulate.Uplink.Templates.Serialization;
using WindowsPoint = System.Windows.Point;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class RectangleBuffer {
		#region Hidden properties
		
		private readonly HashSet<MappingRectangle> _innerList;
		private readonly MyCanvas _canvas;
		private MyPage CurrentPage {
			get { return _canvas.Document.CurrentPage; }
		}
		private bool IsDocumentModified {
			set { _canvas.Document.IsModified = value; }
		}

		#endregion

		public RectangleBuffer(MyCanvas canvas) {
			_innerList = new HashSet<MappingRectangle>();
			_canvas = canvas;
		}

		private Point getMostTopLeft() {
			Point result = _innerList.Count > 0 ? _innerList.OrderBy(mr => mr.TopLeft.X).ThenBy(mr => mr.TopLeft.Y).First().TopLeft : new Point(0, 0);
			return result;
		}

		public void Copy() {
			_innerList.Clear();
			foreach (MyRectangle rect in _canvas.Selection.Items) {
				_innerList.Add(rect.GetMappingRectangle());
			}
		}

		public void Paste(WindowsPoint position) {
			Point topLeft = getMostTopLeft();
			foreach (var mappingRect in _innerList) {
				MyRectangle rect = CurrentPage.AddRectangle(position.X + mappingRect.TopLeft.X - topLeft.X, position.Y + mappingRect.TopLeft.Y - topLeft.Y);
				rect.Width = mappingRect.BottomRight.X - mappingRect.TopLeft.X;
				rect.Height = mappingRect.BottomRight.Y - mappingRect.TopLeft.Y;
				rect.MappingMode = mappingRect.Mode;
				rect.MappingText = mappingRect.Text;
			}
			IsDocumentModified = true;
		}
	}
}