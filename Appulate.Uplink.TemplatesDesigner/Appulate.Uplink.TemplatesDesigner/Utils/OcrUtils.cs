﻿using System;
using System.IO;
using System.Linq;
using Accusoft.FormFixSdk;
using Appulate.Ocr;
using Appulate.Ocr.Forms;
using Appulate.Ocr.Identification;
using Appulate.Uplink.Templates;

namespace Appulate.Uplink.TemplatesDesigner.Utils {
	public class OcrUtils {
		private static readonly string[] Extensions = { ".tiff", ".tif" };
		public static void CreateOcrTemplate(string templatePath, string resourcesFolder, DocumentFormatTemplate<PageFormatTemplate> doc) {
			var dirInfo = new DirectoryInfo(resourcesFolder);
			FileInfo[] tifFiles = dirInfo.GetFiles().Where(f => Extensions.Contains(f.Extension.ToLower())).ToArray();
			if (tifFiles.Length == 0) {
				throw new Exception("No image file with tif extension was found.");
			}

			OcrFormSet formSet = CreateFormSet(templatePath, doc);
			UplinkTemplateConverter.Convert(doc, formSet, resourcesFolder);
		}

		private static OcrFormSet CreateFormSet(string directory, DocumentFormatTemplate<PageFormatTemplate> doc) {
			var formSet = new OcrFormSet(directory, doc.Properties.SyncId);
			formSet.ComparableAcords.Add(
				new OcrAcordType {
					AcordName = doc.Properties.Name,
					AcordVersion = OcrAcordType.GetVersionFormat(doc.Properties.Version)
				});
			using (var identificationProcessor = new IdentificationProcessor(Workspace.FormFix)) {
				identificationProcessor.IdentificationQuality = 100;
				identificationProcessor.IdentificationCertainty = 100;
				identificationProcessor.MinimumIdentificationConfidence = 94;
				identificationProcessor.IdentifyRotated90 = true;
				identificationProcessor.IdentifyRotated180 = true;
				identificationProcessor.IdentifyRotated270 = true;
				formSet.Identification = identificationProcessor.WriteToStream();
			}
			return formSet;
		}
	}
}
