﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Appulate.Uplink.TemplatesDesigner.Repository;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class RectangularSelector : RectangleShapeBase {
		private readonly MyCanvas _canvas;
		private Point _origin;

		protected override MyCanvas Canvas {
			get { return _canvas; }
		}

		private IEnumerable<MyRectangle> CurrentPageRectangles {
			get { return Canvas.Document.CurrentPage.Rectangles; }
		}

		public RectangularSelector(MyCanvas canvas) {
			_canvas = canvas;
			Init();
			canvas.Children.Add(this);
		}

		#region Helper routines

		private void Init() {
			Fill = MyBrushes.SelectorFill;
			Stroke = MyBrushes.SelectorStroke;
			Panel.SetZIndex(this, MyZIndices.RectangularSelector);
			StrokeThickness = 1;
		}

		private void SetBounds(double left, double top, double width, double height) {
			Left = left;
			Top = top;
			Width = width;
			Height = height;
		}

		private static bool HasGeometryIntersection(GeometryHitTestResult geoResult) {
			return geoResult.IntersectionDetail == IntersectionDetail.Intersects || geoResult.IntersectionDetail == IntersectionDetail.FullyInside;
		}

		private void ResetIntersection() {
			foreach (var rect in CurrentPageRectangles) {
				rect.IntersectsSelector = false;
			}
		}

		private void AssignIntersection(Geometry region) {
			var prms = new GeometryHitTestParameters(region);
			VisualTreeHelper.HitTest(Canvas, null, hitTestResult => {
				var geoResult = (GeometryHitTestResult) hitTestResult;
				var rect = hitTestResult.VisualHit as MyRectangle;
				if (rect != null && HasGeometryIntersection(geoResult)) {
					rect.IntersectsSelector = true;
				}
				return HitTestResultBehavior.Continue;
			}, prms);
		}

		private void HoverIntersection() {
			foreach (var rect in CurrentPageRectangles) {
				rect.ToggleHover();
			}
		}

		#endregion

		public void StartDrawing(Point point) {
			_origin = point;
			SetBounds(point.X, point.Y, .0, .0);
			Visibility = Visibility.Visible;
		}

		public void StopDrawing() {
			Visibility = Visibility.Collapsed;
			var intersectedRects = CurrentPageRectangles.Where(r => r.IntersectsSelector);
			if (Canvas.LeftClick.CtrlPressed) {
				Canvas.Selection.Add(intersectedRects);
			} else {
				Canvas.Selection.Select(intersectedRects);
			}
			ResetIntersection();
			HoverIntersection();
		}

		public void Draw(Point to) {
			SetBounds(Math.Min(_origin.X, to.X), Math.Min(_origin.Y, to.Y), Math.Abs(to.X - _origin.X), Math.Abs(to.Y - _origin.Y));
			ResetIntersection();
			AssignIntersection(new RectangleGeometry(new Rect(_origin, to)));
			//HoverIntersection();
		}
	}
}