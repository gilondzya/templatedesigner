﻿using System;
using System.Drawing;
using System.IO;
using Accusoft.FormFixSdk;
using Accusoft.ImagXpressSdk;
using Accusoft.ScanFixXpressSdk;
using Accusoft.SmartZoneOCRSdk;
using Appulate.Ocr;
using Appulate.Ocr.Forms;
using Appulate.Uplink.Templates;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.Templates.Serialization;

namespace Appulate.Uplink.TemplatesDesigner.Utils {
	public class UplinkTemplateConverter {
		private static readonly string OcrZoneOptions;

		static UplinkTemplateConverter() {
			using (SmartZoneOCR szo = Workspace.CreateSmartZoneOcr()) {
				szo.Reader.MinimumCharacterConfidence = 10;
				szo.Reader.RejectionCharacter = '~';
				szo.Reader.Area = new Rectangle(0, 0, 0, 0);
				szo.Reader.CharacterSet = CharacterSet.AllCharacters;
				szo.Reader.CharacterSet.Language = Language.English;
				szo.Reader.FieldType = FieldType.GeneralText;

				szo.Reader.Segmentation.DetectSpaces = true;
				szo.Reader.Segmentation.MaximumBlobSize = 0;
				szo.Reader.Segmentation.MinimumTextLineHeight = 8;
				szo.Reader.Segmentation.MultipleTextLines = true;
				szo.Reader.Segmentation.SplitMergedChars = true;
				szo.Reader.Segmentation.SplitOverlappingChars = true;
				OcrZoneOptions =  szo.WriteToStream();
			}
		}

		private static OmrField CreateOmrField(MappingRectangle rect) {
			var field = new OmrField(rect.Id);
			
			// create Dropout XML
			using (var dropProc = new DropOutProcessor(Workspace.FormFix)) {
				dropProc.DropOutMethod = DropOutMethod.ClipWithoutRescaling;
				dropProc.AllowableMisRegistration = 5;
				dropProc.PerformReconstruction = true;
				dropProc.Area = field.Location;
				field.Construction = dropProc.WriteToStream();
			}

			// create & add OMR XML
			using (var omrProc = new OmrProcessor(Workspace.FormFix)) {
				omrProc.Area = new Rectangle(0, 0, 0, 0);
				omrProc.AnalysisComparisonMethod = OmrAnalysisComparisonMethod.CompareClipToFormModel;
				omrProc.MultiSegmentReadDirection = OmrMultiSegmentReadDirection.Normal;
				omrProc.MarkedBubbleThreshold = 4;
				omrProc.UnmarkedBubbleThreshold = 1;
				omrProc.UnmarkedSegmentResult = "";
				omrProc.MarkScheme = OmrMarkScheme.SingleMark;
				omrProc.BubbleShape = OmrBubbleShape.Rectangle;
				omrProc.Orientation = OmrOrientation.HorizontalSegments;
				omrProc.SingleBubbleAreaAdjustmentMethod = SingleBubbleAreaAdjustmentMethod.UseAreaWithoutAdjustment;
				var sm = new OmrSegmentModel();
				var bm = new OmrBubbleModel("x");
				sm.Bubbles.Add(bm);
				omrProc.Segments.Add(sm);
				field.Recognition = omrProc.WriteToStream();
			}
			return field;
		}

		private static OcrField CreateOcrField(MappingRectangle rect) {
			var field = new OcrField(rect.Id);

			field.Recognition = OcrZoneOptions;

			using (var dropProc = new DropOutProcessor(Workspace.FormFix)) {
				dropProc.DropOutMethod = DropOutMethod.Clip;
				dropProc.AllowableMisRegistration = 5;
				dropProc.PerformReconstruction = true;
				dropProc.Area = field.Location;
				field.Construction = dropProc.WriteToStream();
			}
			return field;
		}

		private static void processPage(OcrForm definition, PageFormatTemplate page) {
			using (Bitmap bitmap = definition.ReadBitmap()) {
				int ocrImageWidth = bitmap.Width;
				int ocrImageHeight = bitmap.Height;
				
				int templateWidth = page.Size.Width;
				int templateHeight = page.Size.Height;

				double scaleX = (double)ocrImageWidth / templateWidth;
				double scaleY = (double)ocrImageHeight / templateHeight;

				foreach (MappingRectangle rect in page.Mappings) {
					OcrTemplateField field;
					if (rect.Mode == MappingMode.IsCheckbox || rect.Mode == MappingMode.AnswerIsNo || rect.Mode == MappingMode.AnswerIsYes || rect.Text.StartsWith("CollectionYesNo(")) {
						field = CreateOmrField(rect);
					} else {
						field = CreateOcrField(rect);
					} 

					if (rect.TopLeft.X < 0) {
						rect.TopLeft.X = 0;
					}
					if (rect.TopLeft.Y < 0) {
						rect.TopLeft.Y = 0;
					}
					if (rect.BottomRight.X < 0) {
						rect.BottomRight.X = 0;
					}
					if (rect.BottomRight.Y < 0) {
						rect.BottomRight.Y = 0;
					}
					int width = rect.BottomRight.X - rect.TopLeft.X;
					field.Location = new Rectangle((int)(rect.TopLeft.X * scaleX), (int)(rect.TopLeft.Y * scaleY), (int)(width * scaleX), (int)((rect.BottomRight.Y - rect.TopLeft.Y) * scaleY));
					if (field.Location.Height == 0 || field.Location.Width == 0) {
						continue;
					}

					definition.AddField(field); 
				}
			}
		}

		public static void Convert(DocumentFormatTemplate<PageFormatTemplate> documentFormatTemplate, OcrFormSet formSet, string resourcesFolder) {
			foreach (PageFormatTemplate page in documentFormatTemplate.Pages) {
				OcrForm form = formSet.GetForm(page.Id);
				if (form == null) {
					var path = Path.Combine(resourcesFolder, page.DevelopmentPicture);
					ValidateImage(path);
					form = new OcrForm(formSet, resourcesFolder, page.DevelopmentPicture, page.Id);
					formSet.AddForm(form);
				}
				processPage(form, page);
			}

			formSet.Save();

			//var formSet = new FormSetFile(OcrZoneWorkspace.FormDirector);
			//formSet.Filename = ocrTemplatePath;
			//formSet.Read();
			//foreach (FormDefinitionFile def in formSet.FormDefinitions) {
			//  def.Read();
			//}
			//var formatTemplate = DocumentFormatHelper.LoadAmsInfo<AmsDocumentFormat>(uplinkTemplatePath);
			//foreach (FormDefinition formDefinition in formSet.FormDefinitions) {
			//	var fd = formDefinition;
			//	var page = documentFormatTemplate.Pages.FirstOrDefault(p => p.DevelopmentPicture == fd.Name + ".tif");
			//	if (page != null) {
			//		processPage(fd, page);
			//	}
			//}
		}

		private static void ValidateImage(string imagePath) {
			using (ImagXpress imgXpress = Workspace.CreateImagXpress()) {
				ImageX img;
				using (var bmp = new Bitmap(imagePath)) {
					img = ImageX.FromBitmap(imgXpress, bmp);
				}
				using (ScanFix scanFix = Workspace.CreateScanFix()) {
					img.CopyTo(scanFix);
					
					if (img.Resolution.Dimensions.Height < 300 || img.Resolution.Dimensions.Width < 300) {
						throw new Exception($"Image resolution is very low: H:{img.Resolution.Dimensions.Height} W{img.Resolution.Dimensions.Width}");
					}
					if (img.BitsPerPixel != 1) {
						throw new Exception("Image resolution is not bitonal");
					}
					if (img.Width != 2550 || img.Height != 3300) {
						throw new Exception($"Size is not valid: {img.Width}x{img.Height}. Must be 2550x3300");
					}
					//bool changed = false;

					//if (img.Resolution.Dimensions.Height != 300 || img.Resolution.Dimensions.Width != 300) {
					//	scanFix.Rescale(2550/(double) img.Width, 3300/(double) img.Height);
					//	scanFix.HorizontalResolution = 300;
					//	scanFix.VerticalResolution = 300;
					//	changed = true;
					//}

					//if (img.BitsPerPixel != 1) {
					//	scanFix.AutoBinarize();
					//	changed = true;
					//}

					//if (changed) {
					//	var enhancements = new Enhancements();
					//	enhancements.Options.Add(new DeskewOptions());
					//	scanFix.ExecuteEnhancements(enhancements);
					//	scanFix.TransferTo(img);
					//	img.Save(Path.Combine(Path.GetDirectoryName(imagePath), "@"+Path.GetFileName(imagePath)));
					//}
					
				}
			}
		}
	}
}
