﻿using System.Configuration;
using System.Data.SqlClient;
using Appulate.Infrastructure.Configuration;

namespace Appulate.Uplink.TemplatesDesigner.Configuration {
	internal class DatabaseSettings : DatabaseSettingsBase {
		private readonly SqlConnectionStringBuilder _connectionString;

		public DatabaseSettings() {
			_connectionString = new SqlConnectionStringBuilder(ConfigurationManager.AppSettings["ConnectionString"]);
		}

		public override string DatabaseServer => _connectionString == null ? string.Empty : _connectionString.DataSource;

		public override string DatabaseName => _connectionString == null ? string.Empty : _connectionString.InitialCatalog;

		public override string DatabaseCredentials => _connectionString == null
			? string.Empty
			: _connectionString.IntegratedSecurity
				? string.Empty
				: string.Concat(_connectionString.UserID, ";", _connectionString.Password);
	}
}
