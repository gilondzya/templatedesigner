﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class ComboItemsBinder {
		public const int ScrollBarWidth = 26;

		public static int BindEnumItems(ComboBox box, Type enumType) {
			box.Items.Clear();
			int itemCount = 0;
			foreach (var item in Enum.GetValues(enumType)) {
				box.Items.Add(new ComboBoxItem { Content = item });
				itemCount++;
			}
			return itemCount;
		}

		public static int BindEnumerableComboBoxItems<T>(ItemsControl itemsControl, IEnumerable<T> items) {
			itemsControl.Items.Clear();
			int itemCount = 0;
			foreach (T item in items) {
				itemsControl.Items.Add(new ComboBoxItem { Content = item });
				itemCount++;
			}
			return itemCount;
		}

		public static int BindEnumerableButtonsItems<T>(ItemsControl itemsControl, IEnumerable<T> items) {
			itemsControl.Items.Clear();
			int itemCount = 0;
			foreach (T item in items) {
				itemsControl.Items.Add(new Button { Width = itemsControl.Width - ScrollBarWidth, Content = item });
				itemCount++;
			}
			return itemCount;
		}
	}
}