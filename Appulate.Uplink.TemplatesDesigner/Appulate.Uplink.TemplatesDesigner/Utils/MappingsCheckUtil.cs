﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Appulate.Uplink.DocumentInfo;
using Appulate.Uplink.Extraction.ParseDocument;
using Appulate.Uplink.Extraction.Pdf;
using Appulate.Uplink.Templates;
using Appulate.Uplink.TextParsing.FlowSections;
using Brushes = System.Windows.Media.Brushes;

namespace Appulate.Uplink.TemplatesDesigner.Utils {
	public static class MappingsCheckUtil {
		private const string FlowSectionMap = "AdditionalPageData";
		private static readonly Regex FlowSectionHeadersRegex = new Regex("\"(?<value>[^\"]*)\"", RegexOptions.Compiled);

		public static MappingsCheckResult Check(PdfRetriveDocument document, IPdfUplinkTemplate template, bool checkpointsOnly) {
			return checkpointsOnly ? GetCheckpointResult(document, template) : GetDocRecognitionResult(document, template);
		}

		private static MappingsCheckResult GetCheckpointResult(PdfRetriveDocument document, IPdfUplinkTemplate template) {
			var results = new List<PdfParsePageResult>();

			DocumentFormatTemplate<PageFormatTemplate> documentFormat = template.LoadDocumentFormat<PageFormatTemplate>();
			var correctionPointOffset = new CorrectionPointOffset(document, documentFormat.MetaInformation);
			PointF offset = correctionPointOffset.Offset;
			int startPage = correctionPointOffset.DocumentStartPage;

			int extraPageStart = startPage;
			int extraPageFormatStart = -1;

			for (int i = 0; i < documentFormat.Pages.Count; i++) {
				if (documentFormat.Pages[i].IsMultiuse) {
					extraPageFormatStart = i;
					break;
				}
			}

			extraPageStart += extraPageFormatStart == -1 ? documentFormat.Pages.Count : extraPageFormatStart;
			for (int i = startPage; i < extraPageStart; i++) {
				PageFormatTemplate pageTemplate = documentFormat.Pages[i - startPage];
				IPdfPage extractedPage = document.Pages[i];
				var pageTextContainer = new PageTextWordContainer(extractedPage);
				CheckPointValidationResult[] validatorResult = CheckPointValidator.GetMatch(pageTextContainer, pageTemplate.CheckPoints, offset);
				results.Add(new PdfParsePageResult(pageTemplate.Id, extractedPage.PageNum, offset, pageTextContainer, validatorResult));
				if (validatorResult.Any(x => !x.Success)) {
					break;
				}
			}

			if ((results.Count > 0 || template.IsAttachment) && extraPageFormatStart >= 0) {
				for (int i = extraPageStart; i < document.Pages.Count; i++) {
					for (int j = extraPageStart; j < documentFormat.Pages.Count; j++) {
						PageFormatTemplate pageTemplate = documentFormat.Pages[j];
						IPdfPage extractedPage = document.Pages[i];
						var pageTextContainer = new PageTextWordContainer(extractedPage);
						CheckPointValidationResult[] validatorResult = CheckPointValidator.GetMatch(pageTextContainer, pageTemplate.CheckPoints, offset);
						if (validatorResult.All(x => x.Success)) {
							results.RemoveAll(x => x.PageIndex == extractedPage.PageNum);
							results.Add(new PdfParsePageResult(pageTemplate.Id, extractedPage.PageNum, offset, pageTextContainer, validatorResult));
							break;
						}
						results.Add(new PdfParsePageResult(pageTemplate.Id, extractedPage.PageNum, offset, pageTextContainer, validatorResult));
					}
				}
			}
			var textBlocks = new List<TextBlock>();
			for (int i = 0; i < results.Count; i++) {
				PdfParsePageResult matchResult = results[i];
				textBlocks.Add(CreateReportLine(matchResult.PageIndex + 1, matchResult.CheckPointValidationResult.All(r => r.Success)));
				textBlocks.Add(new TextBlock());
				textBlocks.AddRange(matchResult.CheckPointValidationResult.Select(CreateReportLine));
				if (i != results.Count - 1) {
					textBlocks.Add(new TextBlock());
				}
			}
			return new MappingsCheckResult(textBlocks, results.All(x => x.CheckPointValidationResult.All(y => y.Success)));
		}

		private static MappingsCheckResult GetDocRecognitionResult(PdfRetriveDocument document, IPdfUplinkTemplate template) {
			PdfParseResult result = template.GetMatchResult(document);
			var textBlocks = new List<TextBlock>();
			var doc = new PdfParseDocument(document, result, Factory.Resolve<IAcordDocumentManager>());
			for (int i = 0; i < doc.Pages.Length; i++) {
				IParsePage page = doc.Pages[i];
				textBlocks.Add(CreateReportLine(page.PageNum + 1, result.Items[i].CheckPointValidationResult.All(r => r.Success)));
				textBlocks.Add(new TextBlock());
				foreach (IParseText text in page.TextBlocks) {
					if (text.MappingText.StartsWith(FlowSectionMap)) {
						IEnumerable<IParseText> newParseTexts = GetFlowSectionParsingResult(page, text);
						textBlocks.AddRange(newParseTexts.Select(CreateReportLine));
					} else {
						textBlocks.Add(CreateReportLine(text));
					}
				}
				if (i != doc.Pages.Length - 1) {
					textBlocks.Add(new TextBlock());
				}
			}
			return new MappingsCheckResult(textBlocks, result.Success);
		}

		private static TextBlock CreateReportLine(int pageNum, bool success) {
			return new TextBlock(new Bold(new Run($"Page {pageNum} - {(success ? "Success" : "Not recognized")}"))) {
				HorizontalAlignment = HorizontalAlignment.Left,
				TextAlignment = TextAlignment.Center,
				Background = Brushes.LightGray,
				Width = 600	
			};
		}

		private static TextBlock CreateReportLine(CheckPointValidationResult text) {
				var block = new TextBlock();
				block.Inlines.Add(new Bold(new Run("MappingText: ")));
				block.Inlines.Add(new Run(text.MappingText));
				block.Inlines.Add(new Bold(new Run(" Text: ")));
				block.Inlines.Add(new Run(text.Text));
				block.Inlines.Add(new Bold(new Run(" Success: ")));
				block.Inlines.Add(new Run(text.Success.ToString()));
				return block;
		}

		private static TextBlock CreateReportLine(IParseText text) {
			var block = new TextBlock();
			block.Inlines.Add(new Bold(new Run("MappingText: ")));
			block.Inlines.Add(new Run(text.MappingText));
			block.Inlines.Add(new Bold(new Run(" Text: ")));
			block.Inlines.Add(new Run(text.Text));
			block.Inlines.Add(new Bold(new Run(" MappingMode: ")));
			block.Inlines.Add(new Run(text.MappingMode.ToString()));
			return block;
		}

		private static IEnumerable<IParseText> GetFlowSectionParsingResult(IParsePage page, IParseText text) {
			IEnumerable<IParseText> result = Enumerable.Empty<IParseText>();
			try {
				string[] headers = FlowSectionHeadersRegex.Matches(text.MappingText).OfType<Match>().Select(m => m.Groups["value"].Value).ToArray();
				IRecognitionWord[] words = text.GetRecognitionWords();
				if (words.Length > 0 && headers.Length > 0) {
					DataTable[] tables = DataTableCreator.CreateDataTables(RecognitionTablesStorage.RecognitionTables, words, headers);
					if (tables.Length == 0) {
						words = text.GetRecognitionWordsWithCoordinatesTransform();
						tables = DataTableCreator.CreateDataTables(RecognitionTablesStorage.RecognitionTables, words, headers);
					}
					result = tables.SelectMany(table => table.GetTextBlocks(page));
				}
			} catch (ArgumentException ex) {
				MessageBox.Show(ex.Message, "ArgumentException", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			return result;
		}
	}
}
