﻿using System.Linq;
using Appulate.Uplink.TextParsing.Utils.ParserResources;
using Appulate.Utilities.State;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class TitleViewData {
		private static string[] _statesViewSource;
		public static string[] StatesViewSource => _statesViewSource ?? (_statesViewSource = new[] { string.Empty }.Concat(StatesCache.AllIds).ToArray());

		public TitleInfo Item { get; set; }

		public TitleViewData(TitleInfo info) {
			Item = info.Copy();
		}
	}
}