﻿using System.Windows.Input;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public partial class MyCanvas {
		private static bool IsCtrl {
			get { return (Keyboard.Modifiers & ModifierKeys.Control) != 0; }
		}

		private static bool IsShift {
			get { return (Keyboard.Modifiers & ModifierKeys.Shift) != 0; }
		}

		private void initKeyEvents() {
			KeyDown += canvas_DeleteKey;
			KeyDown += canvas_ArrowKey;
			KeyDown += canvas_CtrlAKey;
			KeyDown += canvas_CtrlCKey;
			KeyDown += canvas_CtrlSKey;
			KeyDown += canvas_CtrlVKey;
			KeyDown += canvas_CtrlDKey;

			KeyDown += canvas_PageDown;
			KeyDown += canvas_PageUp;
			KeyDown += canvas_Home;
			KeyDown += canvas_End;
		}

		#region PageUp, PageDown, Home and End

		private void canvas_PageDown(object sender, KeyEventArgs e) {
			if (e.Key != Key.PageDown) { return; }
			int pageIndex = Document.CurrentPageIndex + 1;
			if (pageIndex < Document.PageCount) {
				Document.CurrentPageIndex = pageIndex;
			}
			e.Handled = true;
		}

		private void canvas_PageUp(object sender, KeyEventArgs e) {
			if (e.Key != Key.PageUp) { return; }
			int pageIndex = Document.CurrentPageIndex - 1;
			if (pageIndex >= 0) {
				Document.CurrentPageIndex = pageIndex;
			}
			e.Handled = true;
		}

		private void canvas_Home(object sender, KeyEventArgs e) {
			if (e.Key != Key.Home) { return; }
			Document.CurrentPageIndex = 0;
			e.Handled = true;
		}

		private void canvas_End(object sender, KeyEventArgs e) {
			if (e.Key != Key.End) { return; }
			Document.CurrentPageIndex = Document.PageCount - 1;
			e.Handled = true;
		}

		#endregion

		#region Ctrl+C, Ctrl+V, and Ctrl+D(+Shift)

		private void canvas_CtrlCKey(object sender, KeyEventArgs e) {
			if (!IsCtrl || e.Key != Key.C) { return; }
			Buffer.Copy();
			e.Handled = true;
		}

		private void canvas_CtrlSKey(object sender, KeyEventArgs e) {
			if (!IsCtrl || e.Key != Key.S) { return; }
			SaveDocument();
			e.Handled = true;
		}

		private void canvas_CtrlDKey(object sender, KeyEventArgs e) {
			if (!IsCtrl || e.Key != Key.D) { return; }
			Selection.Duplicate(!IsShift);
			e.Handled = true;
		}

		private void canvas_CtrlVKey(object sender, KeyEventArgs e) {
			if (!IsCtrl || e.Key != Key.V) { return; }
			Buffer.Paste(Mouse.GetPosition(this));
			e.Handled = true;
		}

		#endregion

		private void canvas_DeleteKey(object sender, KeyEventArgs e) {
			if (e.IsUp || e.Key != Key.Delete) { return;	}
			Selection.RemoveAll();
			e.Handled = true;
		}

		private void canvas_ArrowKey(object sender, KeyEventArgs e) {
			e.Handled = true;
			double x = .0;
			double y = .0;
			switch (e.Key) {
				case Key.Left:
					x = -1.0;
					break;
				case Key.Up:
					y = -1.0;
					break;
				case Key.Right:
					x = 1.0;
					break;
				case Key.Down:
					y = 1.0;
					break;
				default:
					e.Handled = false;
					return;
			}
			double offset = (IsCtrl) ? 10.0 : 1.0;
			x *= offset;
			y *= offset;
			if (IsShift) {
				Selection.Resize(x, y);
			} else {
				Selection.Move(x, y);
			}
		}

		private void canvas_CtrlAKey(object sender, KeyEventArgs e) {
			if (!IsCtrl || e.Key != Key.A) { return; }
			Selection.Select(CurrentPage.Rectangles);
			e.Handled = true;
		}
	}
}