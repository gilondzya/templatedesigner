﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Utilities.Misc;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public partial class MyCanvas : Canvas {
		private readonly ContextMenu _selectMappingContextMenu = new ContextMenu();
		private Point _lastMousePos;
		private readonly RectangularSelector _selector;
		public MyImage BackgroundImage { get; private set; }
		private MyPage CurrentPage => Document?.CurrentPage;
		public int MappingTextFontSize { get; set; }
		public RectangleSelection Selection { get; private set; }
		public RectangleBuffer Buffer { get; private set; }
		public MyDocument Document { get; set; }
		public MouseLeftClickState LeftClick { get; set; }
		
		public event EventHandler SelectedRectanglesChanged;

		static MyCanvas() {
			DefaultStyleKeyProperty.OverrideMetadata(typeof(MyCanvas), new FrameworkPropertyMetadata(typeof(MyCanvas)));
		}

		public MyCanvas() {
			Background = Brushes.White;
			MappingTextFontSize = 11;
			_selectMappingContextMenu.Opened += ContextMenuOpened;
			ContextMenu = _selectMappingContextMenu;
			BackgroundImage = new MyImage(this);
			_selector = new RectangularSelector(this);
			Selection = new RectangleSelection(this);
			Buffer = new RectangleBuffer(this);

			LeftClick = new MouseLeftClickState();
			MouseLeftButtonDown += canvas_MouseLeftButtonDown;
			MouseLeftButtonUp += canvas_MouseLeftButtonUp;
			MouseLeftButtonUp += (e, a) => InvokeSelectedRectanglesChanged();
			KeyUp += (e, a) => InvokeSelectedRectanglesChanged();
			MouseDown += canvas_MouseDown;
			MouseMove += canvas_MouseMove;
			MouseUp += canvas_MouseUp;

			initKeyEvents();
		}

		private void document_CurrentPageChanging(object sender, System.EventArgs e) {
			var rectangles = Children.OfType<MyRectangle>().ToList();
			foreach (var rect in rectangles) {
				rect.MouseLeftButtonDown -= Rectangle_MouseLeftButtonDown;
				Children.Remove(rect);
			}
		}

		private void document_CurrentPageChanged(object sender, System.EventArgs e) {
			if (CurrentPage == null) {
				return;
			}
			BackgroundImage.Load(CurrentPage.DevelopmentBitmap);
			foreach (var rect in CurrentPage.Rectangles) {
				rect.MouseLeftButtonDown += Rectangle_MouseLeftButtonDown;
				rect.InitMargingBinding();
				Children.Add(rect);
			}		
		}

		private void canvas_MouseDown(object sender, MouseButtonEventArgs e) {
			Mouse.Capture(this, CaptureMode.SubTree);

			if (e.ClickCount == 2 && !LeftClick.RectangleClicked) {
				MyRectangle rect = CurrentPage.AddRectangle(e.GetPosition(this));
				if (IsCtrl) {
					Selection.Add(rect);
				} else {
					Selection.Select(rect);
				}
			}
		}

		private void canvas_MouseUp(object sender, MouseButtonEventArgs e) {
			Focus();
			ReleaseMouseCapture();
		}

		public void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
			var rect = (MyRectangle) sender;
			if (!IsCtrl && !rect.IsSelected) {
				Selection.Select(rect);
			}
			_lastMousePos = e.GetPosition(this);
			LeftClick.Rectangle = rect;
		}

		private void canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
			_lastMousePos = e.GetPosition(this);
			LeftClick.Press(_lastMousePos, IsCtrl);
		}

		private void canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
			Point position = e.GetPosition(this);
			if (LeftClick.RectangleClicked) {
				if (position == LeftClick.PressedAt) {
					if (LeftClick.CtrlPressed) {
						Selection.Toggle(LeftClick.Rectangle);
					} else {
						Selection.Select(LeftClick.Rectangle);
					}
				}
			} else {
				_selector.StopDrawing();
			}
			LeftClick.Release();
		}

		private void canvas_MouseMove(object sender, MouseEventArgs e) {
			if (e.LeftButton != MouseButtonState.Pressed) {
				return;
			}
			Point position = e.GetPosition(this);
			if (LeftClick.RectangleClicked) {
				if (LeftClick.IsFirstMove) {
					Selection.Add(LeftClick.Rectangle);
				}
				if (IsShift) {
					Selection.Resize(_lastMousePos, position);
				} else {
					Selection.Move(_lastMousePos, position);
				}
			} else {
				if (LeftClick.IsFirstMove) {
					if (!IsCtrl) {
						Selection.UnselectAll();
					}
					_selector.StartDrawing(LeftClick.PressedAt);
				} else {
					_selector.Draw(position);
				}
			}
			if (LeftClick.IsFirstMove) {
				LeftClick.IsFirstMove = false;
			}
			_lastMousePos = position;
		}

		public void ContextMenuOpened(object sender, RoutedEventArgs e) {
			_selectMappingContextMenu.Items.Clear();
			Point position = Mouse.GetPosition(this);
			foreach (MyRectangle item in CurrentPage.Rectangles.Where(r => r.PointInside(position))) {
				var menu = new Label { Content = item.MappingText };
				menu.MouseLeftButtonUp += (o, args) => {
					Selection.Select(item);
					InvokeSelectedRectanglesChanged();
				};
				_selectMappingContextMenu.Items.Add(menu);
			}
		}

		public void LoadDevelopmentBitmapImage(BitmapImage bitmapImage) {
			BackgroundImage.Load(bitmapImage);
			foreach (var rect in CurrentPage.Rectangles) {
				rect.InitMargingBinding();
			}
		}

		public void SetDocument(MyDocument document) {
			Document = document;
			Document.CurrentPageChanging += document_CurrentPageChanging;
			Document.CurrentPageChanged += document_CurrentPageChanged;
		}

		public void ClearDocument() {
			Children.Clear();
			BackgroundImage = new MyImage(this);
		}

		public void SaveDocument() {
			try {
				Document.Save();
				MyMessageBox.SimpleMessage("Document saved");
			} catch (System.Exception ex) {
				MyMessageBox.Error(ex.Message);
			}
		}

		public int MakeInvisibleRectanglesWithOutText(string text) {
			int foundCount = 0;
			bool textNotEmpty = !string.IsNullOrEmpty(text);
			foreach (MyRectangle rect in Children.OfType<MyRectangle>()) {
				if (textNotEmpty) {
					rect.Colored = rect.MappingText.ContainsCaseInsensitive(text);
					if (rect.Colored) {
						foundCount++;
					}
				} else {
					rect.Colored = true;
				}
			}
			return foundCount;
		}

		private void InvokeSelectedRectanglesChanged() {
			SelectedRectanglesChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}