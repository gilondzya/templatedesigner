﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Appulate.Business;
using Appulate.Domain.BusinessServices;
using Appulate.Shared.Pdf;
using Appulate.Uplink.Cache;
using Appulate.Uplink.Extraction.Pdf;
using Appulate.Uplink.Extraction.Pdf.PdfNet;
using Appulate.Uplink.Templates;
using Appulate.Uplink.TemplatesDesigner.Controls;
using Appulate.Uplink.TemplatesDesigner.Repository;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Appulate.Uplink.TemplatesDesigner.Utils;
using Microsoft.Win32;
using Doc = WebSupergoo.ABCpdf11.Doc;
using TextElement = System.Windows.Documents.TextElement;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	public partial class ExamineCheckPointsWindow {
		private string _filePath;
		private readonly SolidColorBrush _foundedTextBrush = new SolidColorBrush(Color.FromRgb(0, 202, 255));
		private readonly SolidColorBrush _foundedItemBrush = new SolidColorBrush(Color.FromRgb(210, 246, 255));
		private enum PagesRangeOptions { All, Range }

		public ExamineCheckPointsWindow(IEnumerable<AcordTemplateComboBoxItem> acordTemplates) {
			InitializeComponent();
			ComboItemsBinder.BindEnumerableComboBoxItems(cbAcordTemplates, acordTemplates);
		}

		public void Init(Guid syncId) {
			ComboBoxItem currentTemplate = cbAcordTemplates.Items.Cast<ComboBoxItem>().FirstOrDefault(x => ((AcordTemplateComboBoxItem)x.Content).SyncId == syncId);
			if (currentTemplate != null) {
				currentTemplate.IsSelected = true;
				_filePath = Path.Combine(LoadingControl.UplinkTemplatesResourcesDirectory, currentTemplate.Content.ToString(), syncId + ".pdf");
			} else {
				cbAcordTemplates.SelectedIndex = 0;
			}
			ComboItemsBinder.BindEnumItems(cbPagesRange, typeof(PagesRangeOptions));
			cbPagesRange.Items.Cast<ComboBoxItem>().Single(x => (PagesRangeOptions)x.Content == PagesRangeOptions.All).IsSelected = true;
		}

		private void btnExamine_Click(object sender, RoutedEventArgs e) {
			if (File.Exists(_filePath)) {
				CheckFile(_filePath);
			} else {
				MyMessageBox.Error($"File: {_filePath} not found");
			}
		}

		private void CheckFile(string filePath) {
			Guid syncId = ((AcordTemplateComboBoxItem)((ComboBoxItem)cbAcordTemplates.SelectedItem).Content).SyncId;
			var templateRetriever = Factory.Resolve<IUplinkTemplatesRetriever>();

			IPdfUplinkTemplate template = templateRetriever.SupportedPdfTemplates.Concat(templateRetriever.AttachmentPdfTemplates).FirstOrDefault(x => x.SyncId == syncId);
			if (template == null) {
				Factory.Resolve<IUplinkService>().ResetUplinkTemplateCache();
				template = templateRetriever.SupportedPdfTemplates.Concat(templateRetriever.AttachmentPdfTemplates).FirstOrDefault(x => x.SyncId == syncId);
				if (template == null) {
					MyMessageBox.Error($"Document with SyncId={syncId} not found in UplinkDocument, try Syncronization");
				}
			} else {
				PdfRetriveDocument document = СreateDocumentFromFile(filePath);
				bool onlyCheckPoints = chbOnlyCheckPoints.IsChecked.Value;
				MappingsCheckResult checkResult = MappingsCheckUtil.Check(document, template, onlyCheckPoints);
				lblSuccess.Content = checkResult.IsDocumentRecognized ? "Success" : "Not recognized";
				pnlResult.Children.Clear();
				foreach (TextBlock tempCurItem in checkResult.Report) {
					var curItem = new RichTextBox { BorderBrush = null, IsReadOnly = true };
					var curItemBinding = new Binding { Source = curItem, Path = new PropertyPath("ActualWidth") };
					curItem.Document.SetBinding(FlowDocument.PageWidthProperty, curItemBinding);
					curItem.Document.Blocks.Remove(curItem.Document.Blocks.FirstOrDefault());

					var paragraph = new Paragraph();
					paragraph.Inlines.AddRange(tempCurItem.Inlines.ToArray());
					curItem.Document.Blocks.Add(paragraph);
					pnlResult.Children.Add(curItem);
				}
			}
		}

		private PdfRetriveDocument СreateDocumentFromFile(string filename) {
			byte[] fileContent = File.ReadAllBytes(filename);
			if ((PagesRangeOptions)(cbPagesRange.SelectedItem as ComboBoxItem).Content == PagesRangeOptions.Range) {
				int startPage;
				int endPage;
				if (int.TryParse(tbFrom.Text, out startPage) && int.TryParse(tbTo.Text, out endPage) && startPage <= endPage) {
					Doc doc = AbcPdfWrapper.CreateDocument();
					doc.Read(fileContent);
					if (--startPage >= 0 && --endPage < doc.PageCount) {
						var pagesToRemove = new List<int>();
						for (int i = 0; i < doc.PageCount; i++) {
							if (i < startPage || i > endPage) {
								doc.PageNumber = i + 1;
								pagesToRemove.Add(doc.Page);
							}
						}
						foreach (int page in pagesToRemove) {
							doc.Delete(page);
						}
						fileContent = doc.GetData();
					} else {
						MyMessageBox.Error("Range bounds are out of document pages range");
					}
				} else {
					MyMessageBox.Error("Range bounds are invalid");
				}
			}
			PdfExtractedDocument pdfDocument = PdfExtractFactory.CreateDocument(fileContent);
			var document = new PdfRetriveDocument(pdfDocument);
			return document;
		}

		private int SelectText(RichTextBox curItem, string searchText) {
			int foundCount = 0;
			searchText = searchText.Replace("\\", string.Empty);
			var range = new TextRange(curItem.Document.ContentStart, curItem.Document.ContentEnd);
			range.ApplyPropertyValue(TextElement.BackgroundProperty, value: null);
			if (range.Text.IndexOf(searchText, StringComparison.OrdinalIgnoreCase) >= 0) {
				TextPointer start = curItem.Document.ContentStart;
				while (start != null) {
					if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text) {
						string partText = start.GetTextInRun(LogicalDirection.Forward);
						int indexStart = partText.IndexOf(searchText, StringComparison.OrdinalIgnoreCase);
						if (indexStart >= 0) {
							int indexEnd = indexStart + searchText.Length;
							var findedTextRange = new TextRange(start.GetPositionAtOffset(indexStart, LogicalDirection.Forward),
																									start.GetPositionAtOffset(indexEnd, LogicalDirection.Backward));
							findedTextRange.ApplyPropertyValue(TextElement.BackgroundProperty, _foundedTextBrush);
							start = findedTextRange.End;
							foundCount ++;
						}
					}
					start = start.GetNextContextPosition(LogicalDirection.Forward);
				}
			}
			curItem.Background = foundCount > 0 ? _foundedItemBrush : null;
			return foundCount;
		}

		private void btnOpenFile_Click(object sender, RoutedEventArgs e) {
			var dlg = new OpenFileDialog { Filter = "PDF files (*.pdf)|*.pdf|All files|*.*" };
			if (dlg.ShowDialog() == true) {
				_filePath = dlg.FileName;
			}
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e) {
			if (string.IsNullOrEmpty(tbSearch.Text)) {
				return;
			}
			int foundCount = pnlResult.Children.OfType<RichTextBox>().Sum(curItem => SelectText(curItem, tbSearch.Text));
			tbSearch.Background = foundCount > 0 ? null : Brushes.LightPink;
			tbSeatchCount.Content = foundCount;
		}

		private void pagesRangeVisibleChange(bool isVisible) {
			Visibility visibility = isVisible ? Visibility.Visible : Visibility.Hidden;
			lblFrom.Visibility = visibility;
			tbFrom.Visibility = visibility;
			lblTo.Visibility = visibility;
			tbTo.Visibility = visibility;
			if (visibility == Visibility.Hidden) {
				tbFrom.Text = string.Empty;
				tbTo.Text = string.Empty;
			}
		}

		private void cbPagesRange_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			pagesRangeVisibleChange((PagesRangeOptions)(cbPagesRange.SelectedItem as ComboBoxItem).Content == PagesRangeOptions.Range);
		}

		private void tbSearch_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Return) {
				btnSearch_Click(btnSearch, new RoutedEventArgs());
			}
		}

	}
}
