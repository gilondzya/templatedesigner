﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Xml;
using Appulate.Uplink.Templates;
using Appulate.Uplink.Templates.Data;
using Appulate.Uplink.Templates.Serialization;
using Point = System.Windows.Point;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class MyPage {
		private static readonly BitmapImage NullBitmapImage = new BitmapImage();
		private readonly List<MyRectangle> _innerRectangles;
		private string _pictureFilePath;
		private BitmapImage _developmentBitmap;
		private Guid _id = Guid.Empty;
		private Guid Id {
			get {
				if (_id == Guid.Empty) {
					_id = Guid.NewGuid();
				}
				return _id;
			}
			set { _id = value; }
		}

		public MyPage(MyDocument document) {
			Size = new Size();
			_innerRectangles = new List<MyRectangle>();
			DevelopmentBitmap = NullBitmapImage;
			_pictureFilePath = string.Empty;
			Document = document;
		}

		public MyDocument Document { get; }
		public bool IsMultiuse { get; set; }
		public MyRectangle[] Rectangles => _innerRectangles.ToArray();
		public Size Size { get; }

		public BitmapImage DevelopmentBitmap {
			get { return _developmentBitmap; }
			private set {
				_developmentBitmap?.StreamSource?.Dispose();
				_developmentBitmap = value;
			}
		}

		public string DevelopmentPicture {
			get { return _pictureFilePath; }
			set {
				if (_pictureFilePath == value) {
					return;
				}
				if (string.IsNullOrEmpty(Document.FileName)) {
					DevelopmentBitmap = File.Exists(value) ? new BitmapImage(new Uri(value)) : NullBitmapImage;
					_pictureFilePath = value;
				} else {
					string picture = Path.Combine(Document.GetImagesFolder(), value);
					DevelopmentBitmap = File.Exists(picture) ? new BitmapImage(new Uri(picture)) : NullBitmapImage;
					_pictureFilePath = value;
				}
			}
		}

		public MyRectangle AddRectangle(Point position) {
			var result = new MyRectangle(this) { Top = position.Y, Left = position.X - Document.Canvas.BackgroundImage.LeftMargin, Id = Guid.NewGuid() };
			result.MouseLeftButtonDown += Document.Canvas.Rectangle_MouseLeftButtonDown;
			result.InitMargingBinding();
			Document.Canvas.Children.Add(result);
			_innerRectangles.Add(result);
			Document.IsModified = true;
			return result;
		}

		public MyRectangle AddRectangle(double left, double top) {
			return AddRectangle(new Point(left, top));
		}

		public void RemoveRectangle(MyRectangle rectangle) {
			if (Document.Canvas.Children.Contains(rectangle)) {
				Document.Canvas.Children.Remove(rectangle);
			}
			_innerRectangles.Remove(rectangle);
		}

		public PageFormatTemplate GetPageFormat() {
			var rectanglesList = new List<MappingRectangle>();
			foreach (MyRectangle rect in _innerRectangles) {
				if (rect.MappingMode == MappingMode.Checkpoint && !string.IsNullOrEmpty(rect.MappingText)) {
					rect.MappingText = new string(rect.MappingText.Where(XmlConvert.IsXmlChar).ToArray());
				}
				rectanglesList.Add(rect.GetMappingRectangle());
			}
			var result = new PageFormatTemplate {
				Mappings = rectanglesList.Where(m => m.Mode != MappingMode.Checkpoint).ToArray(),
				CheckPoints = rectanglesList.Where(m => m.Mode == MappingMode.Checkpoint).ToArray(),
				Id = Id,
				IsMultiuse = IsMultiuse
			};
			if (!string.IsNullOrEmpty(DevelopmentPicture)) {
				result.DevelopmentPicture = Path.GetFileName(DevelopmentPicture);
			}
			result.Size.Width = DevelopmentBitmap.UriSource != null ? (int)DevelopmentBitmap.Width : Size.Width;
			result.Size.Height = DevelopmentBitmap.UriSource != null ? (int)DevelopmentBitmap.Height : Size.Height;
			return result;
		}

		public void Load(PageFormatTemplate formatTemplate) {
			_innerRectangles.Clear();
			foreach (MappingRectangle mapping in formatTemplate.AllRectangles) {
				var rect = new MyRectangle(this);
				rect.Init(mapping);			
				_innerRectangles.Add(rect);
			}
			IsMultiuse = formatTemplate.IsMultiuse;
			Size.Height = formatTemplate.Size.Height;
			Size.Width = formatTemplate.Size.Width;
			DevelopmentPicture = formatTemplate.DevelopmentPicture ?? string.Empty;
			Id = formatTemplate.Id;
		}
	}
}