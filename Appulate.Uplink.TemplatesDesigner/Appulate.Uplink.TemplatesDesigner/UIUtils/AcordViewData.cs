﻿using System;
using System.Collections.Generic;
using System.Linq;
using Appulate.Cache;
using Appulate.Uplink.TextParsing.Utils.ParserResources;
using Appulate.Utilities.Collections;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class AcordViewData {
		private static Dictionary<Guid, string> _insurenceTypes;

		public static Dictionary<Guid, string> InsurenceTypes {
			get {
				if (_insurenceTypes == null) {
					_insurenceTypes = new Dictionary<Guid, string> { { Guid.Empty, "NONE" } };
					_insurenceTypes.AddRange(Factory.Resolve<IInsuranceTypeRetriever>().GetAll().ToDictionary(i => i.Id, i => i.Name));
				}
				return _insurenceTypes;
			}
		}
		
		public KeyValuePair<Guid, string> DefaultInsuranceTypeSelected {
			get { return InsurenceTypes.FirstOrDefault(i => i.Key == (Item.DefaultInsuranceType ?? Guid.Empty)); }
			set { Item.DefaultInsuranceType = value.Key; }
		}

		public KeyValuePair<Guid, string> ResolveInsuranceTypeSelected {
			get { return InsurenceTypes.FirstOrDefault(i => i.Key == (Item.ResolveInsuranceType ?? Guid.Empty)); }
			set { Item.ResolveInsuranceType = value.Key; }
		}


		public KeyValuePair<Guid, string> ExtraCoverageLineSelected {
			get { return InsurenceTypes.FirstOrDefault(i => i.Key == (Item.ExtraCoverageLine ?? Guid.Empty)); }
			set { Item.ExtraCoverageLine = value.Key; }
		}

		public AcordPropertiesInfo Item { get; set; }

		public AcordViewData(AcordPropertiesInfo acord) {
			AddIfNotExist(acord.DefaultInsuranceType);
			AddIfNotExist(acord.ResolveInsuranceType);
			AddIfNotExist(acord.ExtraCoverageLine);
			Item = acord;
		}

		private void AddIfNotExist(Guid? id) {
			if (id.HasValue && id != Guid.Empty && !InsurenceTypes.ContainsKey(id.Value)) {
				InsurenceTypes.Add(id.Value, "Unknown");
			}
		}
	}
}