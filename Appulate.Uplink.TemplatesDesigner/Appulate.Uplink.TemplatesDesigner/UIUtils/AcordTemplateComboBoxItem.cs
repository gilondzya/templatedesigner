﻿using System;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	public class AcordTemplateComboBoxItem {
		public string Name { get; set; }
		public Guid SyncId { get; set; }

		public override string ToString() {
			return Name.Replace(' ', '_') + "_" + SyncId;
		}
	}
}