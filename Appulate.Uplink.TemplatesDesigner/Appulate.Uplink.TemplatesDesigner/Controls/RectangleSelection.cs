﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Appulate.Uplink.Templates.Data;
using Size = System.Windows.Size;
using Point = System.Windows.Point;

namespace Appulate.Uplink.TemplatesDesigner.Controls {
	public class RectangleSelection {
		#region Hidden properties

		private static readonly Regex PositionRegex = new Regex("(\"(?<num>[1-9]\\d*)\")", RegexOptions.Compiled);
		private readonly HashSet<MyRectangle> _innerList;
		private readonly MyCanvas _canvas;
		private bool IsDocumentModified {
			set { _canvas.Document.IsModified = value; }
		}

		#endregion

		public RectangleSelection(MyCanvas canvas) {
			_canvas = canvas;
			_innerList = new HashSet<MyRectangle>();
		}

		#region Helper routines

		private void innerToggle(MyRectangle rect, bool isSelected) {
			if (rect.IsSelected == isSelected) {
				return;
			}
			rect.IsSelected = isSelected;
			if (isSelected) {
				_innerList.Add(rect);
			} else {
				_innerList.Remove(rect);
			}
		}

		private Size getBoundingSize() {
			double left = _innerList.Min(i => i.Left);
			double top = _innerList.Min(i => i.Top);
			double right = _innerList.Max(i => i.Left + i.Width);
			double bottom = _innerList.Max(i => i.Top + i.Height);
			return new Size(right - left, bottom - top);
		}

		private MyRectangle duplicateRectangle(MyRectangle source, bool vertically, Size boundingSize) {
			double left = source.Left;
			double top = source.Top;
			if (vertically) {
				top += boundingSize.Height;
			} else {
				left += boundingSize.Width;
			}
			MyRectangle result = _canvas.Document.CurrentPage.AddRectangle(source.Margin.Left + left, top);
			result.Width = source.Width;
			result.Height = source.Height;
			result.MappingMode = source.MappingMode;
			result.XmlPath = source.XmlPath;
			Match m = PositionRegex.Match(source.MappingText);
			if (m.Success) {
				int number = int.Parse(m.Groups["num"].Value);
				result.MappingText = PositionRegex.Replace(source.MappingText, "\"" + (++number).ToString() + "\"");
			} else {
				result.MappingText = source.MappingText;
			}
			return result;
		}

		#endregion

		#region Select and Unselect

		public void Select(MyRectangle rect) {
			UnselectAll();
			innerToggle(rect, true);
		}

		public void Select(IEnumerable<MyRectangle> rects) {
			UnselectAll();
			foreach (var rect in rects) {
				innerToggle(rect, true);
			}
		}

		public void Add(MyRectangle rect) {
			if (!_innerList.Contains(rect)) {
				innerToggle(rect, true);
			}
		}

		public void Add(IEnumerable<MyRectangle> rects) {
			foreach (var rect in rects) {
				innerToggle(rect, true);
			}
		}

		public void Toggle(MyRectangle rect) {
			innerToggle(rect, !rect.IsSelected);
		}

		public void UnselectAll() {
			foreach (var rect in _innerList) {
				rect.IsSelected = false;
			}
			_innerList.Clear();
		}

		public void RemoveAll() {
			foreach (var rect in _innerList) {
				rect.Remove();
			}
			_innerList.Clear();
			IsDocumentModified = true;
		}

		#endregion

		#region Resize and Move

		public void Resize(double offsetX, double offsetY) {
			foreach (var rect in _innerList) {
				rect.Resize(offsetX, offsetY);
			}
			IsDocumentModified = true;
		}

		public void Resize(Point from, Point to) {
			Resize(to.X - from.X, to.Y - from.Y);
		}

		public void Move(double offsetX, double offsetY) {
			foreach (var rect in _innerList) {
				rect.Move(offsetX, offsetY);
			}
			IsDocumentModified = true;
		}

		public void Move(Point from, Point to) {
			Move(to.X - from.X, to.Y - from.Y);
		}

		#endregion

		#region Misc

		public IEnumerable<MyRectangle> Items => _innerList;

		public void ChangeText(string text) {
			foreach (MyRectangle rect in _innerList) {
				rect.MappingText = text;
			}
			IsDocumentModified = true;
		}

		public void ChangeMode(MappingMode mode) {
			foreach (MyRectangle rect in _innerList) {
				rect.MappingMode = mode;
			}
			IsDocumentModified = true;
		}

		public void ChangeInputType(InputType mode) {
			foreach (MyRectangle rect in _innerList) {
				rect.InputType = mode;
			}
			IsDocumentModified = true;
		}

		public void ChangeXmlPath(string xmlPath) {
			foreach (MyRectangle rect in _innerList) {
				rect.XmlPath = xmlPath;
			}
			IsDocumentModified = true;
		}

		public void Duplicate(bool vertically) {
			Size boundingSize = getBoundingSize();
			MyRectangle[] newRectangles = _innerList.Select(r => duplicateRectangle(r, vertically, boundingSize)).ToArray();
			Select(newRectangles);
			IsDocumentModified = true;
		}

		#endregion
	}
}