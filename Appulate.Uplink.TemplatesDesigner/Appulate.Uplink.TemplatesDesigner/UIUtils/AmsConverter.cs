﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Appulate.Business;
using Appulate.Uplink.Consts;
using Appulate.Uplink.Templates.Serialization.Ams;
using Appulate.Uplink.TemplatesDesigner.Controls;

namespace Appulate.Uplink.TemplatesDesigner.UIUtils {
	[ValueConversion(typeof (Collection<Guid>), typeof (string))]
	public class AmsConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			var amss = (Collection<Guid>)value;
			AmsList allAms = AmsList.Load(System.IO.Path.Combine(LoadingControl.UplinkTemplatesDirectory, UplinkFiles.SupportedAmsFile));
			var amsNames = new StringBuilder();
			foreach (var amsItem in allAms.Items.Where(x => amss.Any(y => x.Id == y))) {
				amsNames.AppendFormat("{0}{1}, ", amsItem.Name, !string.IsNullOrEmpty(amsItem.Version) ? " " + amsItem.Version : string.Empty);
			}
			return amsNames.ToString().TrimEnd(',', ' ');
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return DependencyProperty.UnsetValue;
		}
	}
}