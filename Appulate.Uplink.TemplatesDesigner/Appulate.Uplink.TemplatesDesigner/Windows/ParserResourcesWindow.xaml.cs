﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Appulate.Uplink.TemplatesDesigner.UIUtils;
using Appulate.Uplink.TemplatesDesigner.Utils;
using Appulate.Uplink.TextParsing.Utils.ParserResources;

namespace Appulate.Uplink.TemplatesDesigner.Windows {
	public partial class ParserResourcesWindow {
		private readonly CarsInfoList _carMakers;
		private readonly NameNormalizingControlsList _nameNormalizingControls;
		private readonly CollectionAttachmentMaps _collectionAttachmentMaps;
		private readonly CslBiInfoList _cslBiInfoList;

		public ParserResourcesWindow() {
			InitializeComponent();
			_carMakers = new CarsInfoList();
			ParserResoursesUtil.TryLoadParserResource(_carMakers, expCars.Header.ToString());
			dgCarMakers.ItemsSource = _carMakers.Makers.ConvertToBindableList();
			dgCarBody.ItemsSource = _carMakers.BodyTypes.ConvertToBindableList();

			_nameNormalizingControls = new NameNormalizingControlsList();
			ParserResoursesUtil.TryLoadParserResource(_nameNormalizingControls, expNNControls.Header.ToString());
			dgNNControls.ItemsSource = _nameNormalizingControls.ResourcesList.ConvertToBindableList();

			_collectionAttachmentMaps = new CollectionAttachmentMaps();
			ParserResoursesUtil.TryLoadParserResource(_collectionAttachmentMaps, expCAMaps.Header.ToString());
			lvCAMaps.ItemsSource = _collectionAttachmentMaps.ResourcesList.ConvertToBindableList();

			_cslBiInfoList = new CslBiInfoList();
			ParserResoursesUtil.TryLoadParserResource(_cslBiInfoList, expCslBi.Header.ToString());
			lvCslBiMaps.ItemsSource = _cslBiInfoList.ResourcesList.ConvertToBindableList();
		}

		private void btnCAMapsAdd_Click(object sender, RoutedEventArgs e) {
			((ObservableCollection<CollectionAttachmentMap>)lvCAMaps.ItemsSource).Add(new CollectionAttachmentMap());
			e.Handled = true;
		}

		private void btnCAMapsDelete_Click(object sender, RoutedEventArgs e) {
			((ObservableCollection<CollectionAttachmentMap>)lvCAMaps.ItemsSource).Remove(((Button)sender).DataContext as CollectionAttachmentMap);
			e.Handled = true;
		}

		private void btnCslBiAdd_Click(object sender, RoutedEventArgs e) {
			((ObservableCollection<CslBiInfoRow>)lvCslBiMaps.ItemsSource).Add(new CslBiInfoRow());
			e.Handled = true;
		}

		private void btnCslBiDelete_Click(object sender, RoutedEventArgs e) {
			((ObservableCollection<CslBiInfoRow>)lvCslBiMaps.ItemsSource).Remove(((Button)sender).DataContext as CslBiInfoRow);
			e.Handled = true;
		}

		private void btnCarsSave_Click(object sender, RoutedEventArgs e) {
			List<string> makers = ((IEnumerable<StringWrapper>)dgCarMakers.ItemsSource).Select(x => x.Value).ToList();
			makers.Sort();
			_carMakers.Makers = makers;
			List<string> bodyTypes = ((IEnumerable<StringWrapper>)dgCarBody.ItemsSource).Select(x => x.Value).Distinct().ToList();
			bodyTypes.Sort();
			_carMakers.BodyTypes = bodyTypes;
			ParserResoursesUtil.TrySaveParserResource(_carMakers, eventArgs: e);
		}

		private void btnNNControlsSave_Click(object sender, RoutedEventArgs e) {
			_nameNormalizingControls.ResourcesList = ((IEnumerable<StringWrapper>)dgNNControls.ItemsSource).Select(x => x.Value).ToList();
			ParserResoursesUtil.TrySaveParserResource(_nameNormalizingControls, eventArgs: e);
		}

		private void btnCAMapsSave_Click(object sender, RoutedEventArgs e) {
			_collectionAttachmentMaps.ResourcesList = ((ObservableCollection<CollectionAttachmentMap>)lvCAMaps.ItemsSource).ToList();
			ParserResoursesUtil.TrySaveParserResource(_collectionAttachmentMaps, eventArgs: e);
		}

		private void btnCslBiSave_Click(object sender, RoutedEventArgs e) {
			_cslBiInfoList.ResourcesList = ((ObservableCollection<CslBiInfoRow>)lvCslBiMaps.ItemsSource).ToList();
			ParserResoursesUtil.TrySaveParserResource(_cslBiInfoList, eventArgs: e);
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			AcordPropertiesControl.SaveIfHasChanges();
		}
	}
}
